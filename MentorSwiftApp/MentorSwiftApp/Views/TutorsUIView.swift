//
//  TutorsUIView.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 22/09/22.
//

import SwiftUI
import Amplify
import AWSPluginsCore
import AWSAPIPlugin
import Reachability

struct TutorsUIView: View {
    
    @ObservedObject var vm =  ModelTutor()
    
    @State var allUser: [Usuario] = []
    @Environment(\.defaultMinListRowHeight) var minRowHeight
    
    var reachability = try! Reachability()
    @State var ratingSortOnl = SortBy.ratingASConl
    @State var ratingSortOff = SortBy.ratingASCoff

    
    //@State var tutors: [Usuario] = []
    
    
    var body: some View {
        if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
            ZStack{
                Text("Your network is unavailable. Check your data or wifi connection").foregroundColor(.red)
            }
        }
        VStack{
            Text("Find a Tutor").bold().font(.headline)
            List{
                if (NetworkManager.sharedInstance.reachability).connection != .unavailable {
                    /*
                        Text(vm.infoMessage)
                            .font(.headline)
                            .foregroundColor(.purple )
                            .frame(width: UIScreen.main.bounds.width / 1.25)
                            .transition(AnyTransition.move(edge: .bottom).combined(with: .opacity))
                            .onAppear{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                    withAnimation{
                                        self.show = false
                                    }
                                }
                            }
                     */
                        Text(ratingSortOnl.rawValue)
                        .onTapGesture {
                            if (ratingSortOnl == .ratingASConl){
                                ratingSortOnl = .ratingDESConl
                            } else {
                                ratingSortOnl = .ratingASConl
                            }
                            sortList(by: ratingSortOnl)
                        }
                    ForEach(vm.tutors, id: \.identifier) { tutor in
                        TutorsCell(mentor: tutor)
                    }
                }
                else{
                    Text(ratingSortOff.rawValue)
                        .onTapGesture {
                            if (ratingSortOff == .ratingASCoff){
                                ratingSortOff = .ratingDESCoff
                            } else {
                                ratingSortOff = .ratingASCoff
                            }
                            sortList(by: ratingSortOff)
                        }
                    /*
                    Text(vm.infoMessage)
                        .font(.headline)
                        .foregroundColor(.purple )
                     */
                    ForEach(vm.allUser, id: \.identifier) { tutor in
                        TutorsCell(mentor: tutor)
                    }
                }
                
            }
        }.onAppear(perform:{
            NetworkManager.isReachable{ _ in
                vm.listTutors()
                vm.saveToCache()

            }
            NetworkManager.isUnreachable{ _ in
                vm.updateData()
                vm.getUserList()
                vm.getFromCache()
                
            }
        })
    }
    
    
    private func sortList(by sortType: SortBy) {
        switch sortType {
        case .ratingDESConl:
            vm.tutors.sort {
                $0.calificacion ?? 0.0 < $1.calificacion ?? 0.0
            }
        case .ratingDESCoff:
            vm.allUser.sort {
                $0.calificacion ?? 0.0 < $1.calificacion ?? 0.0
            }
        case .ratingASConl:
            vm.tutors.sort {
                $0.calificacion ?? 0.0 > $1.calificacion ?? 0.0
            }
        case .ratingASCoff:
            vm.allUser.sort {
                $0.calificacion ?? 0.0 > $1.calificacion ?? 0.0
            }
        }
    }
    
    enum SortBy: String {
        case ratingASConl = "Sort by the tutor's rating △"
        case ratingASCoff = " Sort by the tutor's rating △"
        case ratingDESConl = "Sort by the tutor's rating ▽"
        case ratingDESCoff = " Sort by the tutor's rating ▽"
    }
    
    private func listTutors() {
        DispatchQueue.global(qos:.userInteractive).async {
            Amplify.API.query(request: .paginatedList(Usuario.self)){ event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todos):
                        print("Successfully retrieved list of todos: \(todos)")
                        vm.tutors = todos.filter({tutor in
                           
                              
                            return tutor.esProfesor == true
                     
                        })
                        //vm.updateData()
                        //vm.insertData()
                        
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
        }
    }
    
    private func insertData(){
        TutorDataStore.shared.deleteAllUsers()
        for tutor in vm.tutors{
            TutorDataStore.shared.insert(id: tutor.id, nombre: tutor.nombre_apellido ?? "", email: tutor.email ?? "", biografia: tutor.biografia ?? "", calificacion: tutor.calificacion ?? 0.0, esProfesor: tutor.esProfesor ?? false , username: tutor.username ?? "")
        }
    }
    
    private func updateData(){
        TutorDataStore.shared.deleteAllUsers()
        for tutor in vm.tutors{
            TutorDataStore.shared.update(id: tutor.id, nombre: tutor.nombre_apellido ?? "", email: tutor.email ?? "", biografia: tutor.biografia ?? "", calificacion: tutor.calificacion ?? 0.0, esProfesor: tutor.esProfesor ?? false , username: tutor.username ?? "")
        }
    }
    
    private func deleteAll(){
        for tutor in vm.tutors{
            TutorDataStore.shared.delete(id: tutor.id)
        }
    }
    
    private func getUserList() {
        allUser = TutorDataStore.shared.getAllUsers()
    }
}

    
struct TutorsCell : View {
    let mentor: Usuario
    var body: some View {
        return NavigationLink(destination: TutorsUIViewDetail(tutor: mentor)) {
            Image("default")
                .resizable()
                .scaledToFit()
                .frame(width: 100,height: 100)
                .cornerRadius(10)
            VStack(alignment: .leading) {
                Text(mentor.nombre_apellido!)
                    .bold()
                Text("Rating: \(mentor.calificacion ?? 0.0, specifier: "%.1f") / 5,0")
                    .font(.subheadline)
                    .padding(.top,1)
                Text(mentor.email ?? "correo")
                    .font(.subheadline)
                    .padding(.top,1)
                    .foregroundColor(.gray)
            }
        }
    }
}
