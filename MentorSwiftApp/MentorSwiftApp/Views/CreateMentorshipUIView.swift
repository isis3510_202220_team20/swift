//
//  CreateMentorshipUIView.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 5/10/22.
//

import Amplify
import SwiftUI
import Foundation
import AWSPluginsCore
import Reachability



struct CreateMentorshipUIView: View {
    
    @ObservedObject var model = ModelCreateMentorship(limit: 25)
    
    @ObservedObject var modelDescription = ModelCreateMentorship(limit: 50)
    
    @ObservedObject var modelValueCapacity = ModelCreateMentorship(limit: 3)
    
    @ObservedObject var modelValuePrice = ModelCreateMentorship(limit: 6)
    
    var reachability = try! Reachability()
    
    @State var alertType: MyAlerts? = nil
    @Environment(\.presentationMode) var presentationMode
    
    enum MyAlerts{
        case success
        case error
    }
    @State var showAlert: Bool = false
    
    var closedRange = Calendar.current.date(byAdding: .year, value:-1, to: Date())!
    
    
    let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    
    var body: some View {
        VStack{
            Text("Create Mentorship").bold().font(.headline)
            VStack {
                Form {
                    Section(header:Text("Mentorship Information")){
                        TextField("Name", text: $model.name)
                            .validation(model.nameValidation)
                            .validation(model.belowTheLimitName)
                            .validation(model.aboveTheLimitName)
                            .validation(model.nameLetters)
                        TextField("Category", text: $model.topic)
                            .validation(model.tutorsNameValidation)
                            .validation(model.belowTheLimitTopic)
                            .validation(model.aboveTheLimitTopic)
                            .validation(model.nameTutorLetters)
                        TextField("Description", text:$modelDescription.description)
                            .validation(modelDescription.descriptionValidation)
                            .validation(modelDescription.belowTheLimitDescription)
                            .validation(modelDescription.aboveTheLimitDescription)
                    }
                    Section(header: Text("Details")){
                        Toggle("Virtual-based modality", isOn: $model.isVirtual)
                        if model.isVirtual{
                            TextField("Platform", text: $modelDescription.location)
                                .validation(modelDescription.locationValidation)
                                .validation(modelDescription.belowTheLimitLocation)
                                .validation(modelDescription.aboveTheLimitLocation)
                        }
                        else{
                            TextField("City", text:$model.city)
                                .validation(model.cityValidation)
                                .validation(model.belowTheLimitCity)
                                .validation(model.aboveTheLimitCity)
                                .validation(model.cityLetters)
                            TextField("Location", text: $modelDescription.location)
                                .validation(modelDescription.locationValidation)
                                .validation(modelDescription.belowTheLimitLocation)
                                .validation(modelDescription.aboveTheLimitLocation)
                        }
                        TextField("Student Capacity", text: $modelValueCapacity.capaci)
                            .keyboardType(.numberPad)
                            .validation(modelValueCapacity.capacityValidation)
                        TextField("Price on COP", text: $modelValuePrice.value)
                            .keyboardType(.numberPad)
                            .validation(modelValuePrice.priceValidation)
                        
                    }
                    Section(header: Text("Date and time")){
                        DatePicker("From",selection: $model.dateBegin, in: Date()...)
                        DatePicker("To",selection: $model.dateEnd, in:model.dateBegin...)
                    }
                    Section{
                        VStack{
                            Button(
                                action: {
                                    DispatchQueue.global(qos:.background).async {
                                        model.createMentorship(nombre: model.name, descripcion: modelDescription.description, topic: model.topic, fechaInicio: Temporal.DateTime(model.dateBegin), fechaFin: Temporal.DateTime(model.dateEnd), ciudad: model.city, lugar: modelDescription.location, precio: Double(modelValuePrice.value)!, capacidad: Int(modelValueCapacity.capaci)!, esVirtual: model.isVirtual)
                                    }
                                    DispatchQueue.main.async {
                                        showAlert.toggle()
                                        alertType = .success
                                        clearAll()
                                    }
                                    
                                    
                                },
                                label: {
                                    Text("Create Mentorship")
                                        .padding()
                                        .foregroundColor(.white)
                                        .background(buttonColor)
                                        .cornerRadius(10)
                                }).position(x:170,y: 18)
                                .frame(width: 200,height: 37)
                                .disabled(!formValid)
                            
                            
                        }.alert(isPresented: $showAlert , content: {
                            getAlert()})
                    }
                    
                    if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
                        ZStack{
                            Text("Your network is unavailable. Check your data or wifi connection. You can create the mentorship once you are back online.").foregroundColor(.red)
                        }.frame(width:300, height: 100)
                            .cornerRadius(20)
                    }
                }
                    
            }.onTapGesture {
                hideKeyboard()
            }
        }
    }
    
    var formValid: Bool{
        if model.isVirtual{
            return !model.name.isEmpty && !modelDescription.description.isEmpty && !model.topic.isEmpty && !modelDescription.location.isEmpty && !modelValuePrice.value.isEmpty && !modelValueCapacity.capaci.isEmpty && (NetworkManager.sharedInstance.reachability).connection != .unavailable
        }
        else{
            return !model.name.isEmpty && !modelDescription.description.isEmpty && !model.topic.isEmpty && !model.city.isEmpty && !modelDescription.location.isEmpty && !modelValuePrice.value.isEmpty && !modelValueCapacity.capaci.isEmpty && (NetworkManager.sharedInstance.reachability).connection != .unavailable
        }
    }
    
    var buttonColor: Color {
        return formValid ? Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)) : .gray
    }
    
    private func clearAll(){
        self.model.name = ""
        self.modelDescription.description = ""
        self.model.topic = ""
        self.model.dateBegin = Date.now
        self.model.dateEnd = Date.now
        self.model.city = ""
        self.modelDescription.location = ""
        self.modelValuePrice.value = ""
        self.modelValueCapacity.capaci = ""
        self.model.isVirtual = false
    }
    
    private func getAlert() -> Alert{
        switch alertType{
        case.error:
            return Alert(title: Text("An error occured!"))
        case.success:
            return Alert(title: Text("Mentorship succesfully created!"), message: nil, dismissButton: .default(Text("Ok"), action: {backHome()})
            )
        default:
            return Alert(title: Text("Mentorship succesfully created!"), message: nil, dismissButton: .default(Text("Ok"), action: {backHome()})
            )
        }
    }
    
    private func backHome() {
        presentationMode.wrappedValue.dismiss()
    }
}



#if canImport(UIKit)
extension CreateMentorshipUIView {
    private func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

