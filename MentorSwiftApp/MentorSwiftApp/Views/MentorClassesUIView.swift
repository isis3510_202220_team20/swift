//
//  MarketplaceUIView.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import SwiftUI
import Amplify
import AWSPluginsCore
import AWSAPIPlugin
import Reachability


struct MentorClassesUIView: View {
    
    @State var mentorships: [Mentoria] = []
    @State var AllMentorships: [Mentoria] = []
    @StateObject var vm = ModelMentorClass()

    
    let tutor: Usuario
    var reachability = try! Reachability()
    
    var body: some View {
        if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
            ZStack{
                Text("Your network is unavailable. Check your data or wifi connection").foregroundColor(.red)
                /*
                Text(vm.infoMessage)
                    .font(.headline)
                    .foregroundColor(.purple )
                 */
            }
        }
        VStack {
            Text((tutor.nombre_apellido ?? "nombre") + "'s classes").bold().font(.headline)
            List{
                if(NetworkManager.sharedInstance.reachability).connection != .unavailable{
                    if(mentorships.isEmpty){
                        Text("The mentor currently doesn't have any mentorships.")
                    }
                    else{
                        /*
                        Text(vm.infoMessage)
                            .font(.headline)
                            .foregroundColor(.purple )
                         */
                        ForEach(mentorships) { mentorship in
                            ClassesDetail(subject: mentorship, tutor: tutor, mentorshipDate: mentorship.fechaInicio)
                        }
                    }
                }
                else{
                    if(AllMentorships.isEmpty){
                        Text("The mentor currently doesn't have any mentorships.")
                    }
                    else{
                        ForEach(AllMentorships) { mentorship in
                            ClassesDetail(subject: mentorship, tutor: tutor, mentorshipDate: mentorship.fechaInicio)
                        }
                    }
                }
            }
        }.onAppear(perform: {
            NetworkManager.isReachable{ _ in
                listMentorships()
                vm.saveToCache()
            }
            NetworkManager.isUnreachable{ _ in
                updateData()
                getMentorshipList()
                vm.getFromCache()
            }
        })
    }
    private func listMentorships() {
        DispatchQueue.global(qos:.background).async {
            Amplify.API.query(request: .paginatedList(Mentoria.self)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todos):
                        print("Successfully retrieved list of todos: \(todos)")
                        
                        self.mentorships = todos.filter({mentorship in
                            tutor.id == mentorship.usuarioID && mentorship.fechaInicio!.foundationDate >= Date.now
                        })
                        updateData()
                        insertData()
                        
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
        }
    }
    
    private func insertData(){
        for mentorship in mentorships {
            MentorsClassesDataStore.shared.insert(id: mentorship.id, nombre: mentorship.nombre ?? "", descripcion: mentorship.descripcion ?? "", topic: mentorship.topic ?? "", fechaInicio: mentorship.fechaInicio!.foundationDate, fechaFin: mentorship.fechaFin!.foundationDate, ciudad: mentorship.ciudad ?? "", lugar: mentorship.lugar ?? "", capacidad: mentorship.capacidad ?? 0, precio: mentorship.precio ?? 0.0, esVirtual: mentorship.esVirtual ?? false, usuarioID: mentorship.usuarioID )
        }
    }
    private func updateData(){
        for mentorship in mentorships {
            MentorsClassesDataStore.shared.update(id: mentorship.id, nombre: mentorship.nombre ?? "", descripcion: mentorship.descripcion ?? "", topic: mentorship.topic ?? "", fechaInicio: mentorship.fechaInicio!.foundationDate, fechaFin: mentorship.fechaFin!.foundationDate, ciudad: mentorship.ciudad ?? "", lugar: mentorship.lugar ?? "", capacidad: mentorship.capacidad ?? 0, precio: mentorship.precio ?? 0.0, esVirtual: mentorship.esVirtual ?? false, usuarioID: mentorship.usuarioID )
        }
    }
    
    private func getMentorshipList(){
        AllMentorships = MentorsClassesDataStore.shared.getAllClasses()
    }
}



struct ClassesDetail : View {
    @StateObject var vm = ModelMentorClass()
    let subject: Mentoria
    let tutor: Usuario
    let mentorshipDate: (Temporal.DateTime?)
    
    var mentorshipBeginDate: String? {
        guard let mentorshipDate = self.mentorshipDate else { return nil }
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
            
        let checkIn = formatter.string(from: mentorshipDate.foundationDate)
            return "\(checkIn)"
    }
    var body: some View {
        return NavigationLink(destination: MarketplaceUIViewDetail(subject:subject, tutor: tutor), label: {
            if let image = vm.startingImage{
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100,height: 100)
                    .cornerRadius(10)
            }
            VStack(alignment: .leading) {
                Text(subject.nombre!)
                    .bold()
                Text(subject.descripcion!)
                    .font(.subheadline)
                    .padding(.top,1)
                Text(mentorshipBeginDate!)
                    .font(.subheadline)
                    .padding(.top,1)
                    .foregroundColor(.gray)
            }
        })
        
    }
}
