//
//  EditProfileUIView.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 12/12/22.
//

import SwiftUI
import Amplify
import AWSCognitoAuthPlugin

class EditProfileViewModel: ObservableObject{
    
    func save(user:Usuario){
        Amplify.API.mutate(request: .update(user)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todo):
                    CurrentUserService.instance.update()
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
    }
    
}


struct EditProfileUIView: View {
    var returnFn: ()->Void
    @StateObject var viewModel = EditProfileViewModel()

    @State var usuario: Usuario = Usuario()
    @State private var nombre: String = ""
    @State private var bio: String = ""
    
    @State private var showModal = false
    
    var body: some View {
        ZStack{
            

        VStack{
            Text("Editing Profile")
                .bold()
                .font(.title)
            Spacer()
            VStack{
                Image("default")
                    .resizable()
                    .frame(width: 120, height: 120)
                    .clipShape(Circle())
                Text(usuario.username ?? "Loading")
                    .font(.title)
                    .bold()
            }
            Spacer().frame(height: 30)
            Form{
                Section(header: Text("Biography")){
                    TextField("Name", text:$nombre)
                }
                Section(header: Text("User Info")){
                    TextEditor(text: $bio)
                        .frame(height:250)
                    HStack {
                        Spacer()
                        Button(action:{
                            print("Bio")
                            showModal = true
                        } , label: {
                            Text("Generate Bio")
                                .frame(width: 260, height: 30)
                                .cornerRadius(10)
                                .foregroundColor(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10).stroke(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)), lineWidth: 1 ))
                        })
                        Spacer()
                    }
                }
            }
                HStack{
                    Spacer().frame(width: 30)
                    Button(action:{
                        usuario.nombre_apellido = nombre
                        usuario.biografia = bio
                        viewModel.save(user: usuario)
                        returnFn()
                    } , label: {
                        Text("Save")
                            .bold()
                            .frame(maxWidth: .infinity, maxHeight: 50)
                            .background(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)))
                            .cornerRadius(10)
                            .foregroundColor(.white)
                    })
                    
                    Spacer().frame(width: 30)
                    Button(action:{
                       returnFn()
                    } , label: {
                        Text("Cancel")
                            .bold()
                            .frame(maxWidth: .infinity, maxHeight: 50)
                            .background(Color.red)
                            .cornerRadius(10)
                            .foregroundColor(.white)
                    })
                    Spacer().frame(width: 30)
                }
               
         
            Spacer().frame(height: 30)
            
        }.onAppear(perform: {loadData()})
            ModalView(isShowing: $showModal, bio: $bio)
        }
    }
    func loadData(){
        self.usuario = CurrentUserService.instance.getCurrentUser()!
        if(usuario.biografia != nil){
            bio = usuario.biografia!
        }
        if(usuario.nombre_apellido != nil){
            nombre = usuario.nombre_apellido!
        }
    }
    
    
}
/*
struct EditProfileUIView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileUIView()
    }
}*/
