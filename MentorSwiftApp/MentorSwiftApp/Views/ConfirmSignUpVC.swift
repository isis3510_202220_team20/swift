//
//  confirmSignUpVC.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 5/10/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin

class ConfirmSignUpVC: UIViewController{
    
    var email: String = ""
    var username: String = ""
    
    
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var errorCodeLBL: UILabel!
    
    @IBOutlet weak var codeTF: UITextField!
    
    @IBOutlet weak var confirmBTN: UIButton!
    
    @IBOutlet weak var anotherCodeBTN: UIButton!
    
    let sucessAlert = UIAlertController(title: "Success",
                                  message: "Your account was created",
                                  preferredStyle: .alert)
    
    let newCodeAlert = UIAlertController(title: "Success",
                                         message: "A new code was sent",
                                  preferredStyle: .alert)
    
    let errorAlert = UIAlertController(title: "Error",
                                  message: "Please try again",
                                  preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetForm()
        sucessAlert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: {_ in self.pushSignInViewController() }))
        
        newCodeAlert.addAction(UIAlertAction(title: "OK",
                                             style: .default,
                                             handler: {_ in self.resetForm()}))
        
        errorAlert.addAction(UIAlertAction(title: "Retry",
                                      style: .default,
                                      handler: {_ in self.resetForm() }))
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    @IBAction func confirmCodeAction(_ sender: Any) {
        confirmSignUp(for: username, with: codeTF.text!)
    }
    
    @IBAction func requestCodeAction(_ sender: Any) {
        requestCode(for: username)
    }
    
    func resetForm(){
        emailLBL.text = "Email: " + email
        confirmBTN.isEnabled = false
        
        errorCodeLBL.isHidden = true
        
        codeTF.text = ""
    }
    
    @IBAction func codeChanged(_ sender: Any) {
        if let code = codeTF.text
        {
            if let errorMessage = invalidCode(code)
            {
                errorCodeLBL.text = errorMessage
                errorCodeLBL.isHidden = false
            }
            else{
                errorCodeLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    func invalidCode(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count != 6 {
            return "Code should be 6 characters"
        }
        return nil
    }
    
    func checkForValidForm(){
        if errorCodeLBL.isHidden{
            confirmBTN.isEnabled = true
        }
        else{
            confirmBTN.isEnabled = false
        }
    }
    
    func confirmSignUp(for username: String, with confirmationCode: String) {
        Amplify.Auth.confirmSignUp(for: username, confirmationCode: confirmationCode) { result in
            switch result {
            case .success:
                print("Confirm signUp succeeded")
                self.presentSuccessAlert()
            case .failure(let error):
                print("An error occurred while confirming sign up \(error)")
                self.presentErrorAlert()
            }
        }
    }
    
    func requestCode(for username: String){
        Amplify.Auth.resendSignUpCode(for: username){ result in
            switch result {
            case .failure(let error):
                print("An error occurred while confirming sign up \(error)")
                self.presentErrorAlert()
            case .success(let details):
                print("Resend code succeeded")
                print(details)
                self.presentNewCodeAlert()
            }
        }
    }
    
    func presentSuccessAlert(){
        DispatchQueue.main.async {
            self.present(self.sucessAlert, animated: true, completion: nil)
        }
    }
    
    func presentNewCodeAlert(){
        DispatchQueue.main.async {
            self.present(self.newCodeAlert, animated: true, completion: nil)
        }
    }
    
    func presentErrorAlert(){
        DispatchQueue.main.async {
            self.present(self.errorAlert, animated: true, completion: nil)
        }
    }
    
    func pushSignInViewController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let signInViewController = storyboard.instantiateViewController(identifier: "SignInVC")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(signInViewController)
        }
    }
}
