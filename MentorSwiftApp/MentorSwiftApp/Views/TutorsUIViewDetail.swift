//
//  TutorsUIViewDetail.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 22/09/22.
//

import Amplify
import SwiftUI

struct TutorsUIViewDetail: View {

    let tutor: Usuario
    
    var body: some View {
        VStack(alignment: .center) {
            Group{
                //profile picture
                Image("default")
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
                    .offset(x: -100 ,y: -40)
                    .frame(width: 170,height: 170)
                    .shadow(radius: 10)
                //name
                Text(tutor.nombre_apellido!)
                    .font(.title)
                    .offset(x:100,y:-220)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.center)
                    .frame(width: 150)
            }
            //email
            Group{
                Text("Email")
                    .font(.subheadline)
                    .offset(x:100,y:-190)
                    .foregroundColor(.gray)
                Text(tutor.email ?? "correo")
                    .font(.subheadline)
                    .offset(x:100,y:-180)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.center)
                    .frame(width: 170)
            }
            //rating
            Group{
                Text("Average rating")
                    .font(.subheadline)
                    .offset(x:100,y:-170)
                    .foregroundColor(.gray)
                Text("\(tutor.calificacion ?? 0, specifier: "%.1f") / 5,0")
                    .font(.subheadline)
                    .offset(x:100,y:-160)
            }
            //topics
            Group{
                Text("Username")
                    .font(.subheadline)
                    .offset(x:100,y:-150)
                    .foregroundColor(.gray)
                Text(tutor.username!)
                    .frame(maxWidth:200,maxHeight: 50)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.leading)
                    .offset(x:100,y:-160)
                    

            }
            Divider().offset(x:0,y:-150)
            //biography
            Group{
                Text("Biography:")
                    .offset(x:0,y:-140)
                    .font(.headline)
                Text(tutor.biografia ?? "To be completed")
                    .offset(x:0,y:-130)
                    .lineLimit(4)
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
                    .frame(width:300)
                    .fixedSize(horizontal: false, vertical: true)
                    
            }
            Divider().offset(x:0,y:-120)
            //mentorships given
            /*
            Group{
                Text("Mentorships given:")
                    .offset(x:0,y:-110)
                    .font(.headline)
                Text("-")
                    .offset(x:0,y:-100)
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
            }
            //upcoming mentorship
            Group{
                Text("Upcoming Mentorship:")
                    .offset(x:0,y:-90)
                    .font(.headline)
                Text("_")
                    .offset(x:0,y:-80)
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
            }
             */
            VStack{
                NavigationLink(destination: MentorClassesUIView(tutor: tutor)) {
                    Text("See mentor's classes")
                        .padding()
                        .foregroundColor(.white)
                        .background(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)))
                        .cornerRadius(10)
                }.offset(x:0,y:-20).font(.headline)
            }
        }.padding().navigationBarTitle(Text(tutor.nombre_apellido!), displayMode: .inline)
    }
}

