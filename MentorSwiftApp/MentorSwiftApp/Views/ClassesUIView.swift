//
//  ClassesUIView.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import SwiftUI
import Amplify
import AWSPluginsCore

struct ClassesUIView: View {
    var subjects: [Classes] = []
    @State var mentorships: [Mentoria] = []
    
    var body: some View {
        VStack {
            Text("My Classes").bold().font(.title)
            List{
                ForEach(mentorships) { mentorship in
                    MyClassesCell(subject: mentorship, mentorshipDate: mentorship.fechaInicio)
                }
            }.background(Color.white)
        }.onAppear(perform: {
            listMyClasses()
        })
    }
    
    func listMyClasses() {
        DispatchQueue.global(qos:.background).async {
            let currentUser: Usuario = CurrentUserService.instance.getCurrentUser()!
            let mentorias = currentUser.EstudiantesMentoria?.load().elements.map({ relacion -> Mentoria in
                return relacion.mentoria
            })
            print(mentorias)
            if(mentorias != nil && mentorias!.count > 0){
                self.mentorships = mentorias!
            }

        }
    }
}

struct MyClassesCell : View {
    let subject: Mentoria
    let mentorshipDate: (Temporal.DateTime?)
    
    var mentorshipBeginDate: String? {
        guard let mentorshipDate = self.mentorshipDate else { return nil }
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        
        let checkIn = formatter.string(from: mentorshipDate.foundationDate)
        return "\(checkIn)"
    }
    var body: some View {
        return NavigationLink(destination: ClassesUIViewDetail(subject:subject), label: {
            Image("marketplace")
                .resizable()
                .scaledToFit()
                .frame(width: 100,height: 100)
                .cornerRadius(10)
            VStack(alignment: .leading) {
                Text(subject.nombre!)
                    .bold()
                Text(subject.descripcion!)
                    .font(.subheadline)
                    .padding(.top,1)
                Text(mentorshipBeginDate!)
                    .font(.subheadline)
                    .padding(.top,1)
                    .foregroundColor(.gray)
            }
        })
    }
}

extension GraphQLRequest {
    static func getMyClassesQueryFake(byId id: String) -> GraphQLRequest<[Mentoria]> {
        let operationName = "listUsuarioMentorias"
        let document = """
        query getStudentClasses($eq: ID!) {
          \(operationName)(filter: {usuarioID: {eq: $eq}}) {
            items {
              mentoria {
                id
                capacidad
                ciudad
                createdAt
                descripcion
                esVirtual
                fechaFin
                fechaInicio
                lugar
                nombre
                precio
                topic
                usuarioID
              }
            }
          }
        }
    """
        return GraphQLRequest<[Mentoria]>(
            document: document,
            variables: ["eq": id],
            responseType: [Mentoria].self,
            decodePath: operationName
        )
    }
}
