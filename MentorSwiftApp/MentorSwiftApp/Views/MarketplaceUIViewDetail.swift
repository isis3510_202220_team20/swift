//
//  MarketplaceUIViewDetail.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import SwiftUI
import Amplify

struct MarketplaceUIViewDetail: View {
    
    @State var showAlert: Bool = false
    @State var alertType: MyAlerts? = nil
    @State var isReserved: Bool = false
    let subject: Mentoria
    var tutor: Usuario
    
    var mentorshipDates: String? {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        
        let fechaInicio = formatter.string(from: subject.fechaInicio!.foundationDate)
        let fechaFin = formatter.string(from: subject.fechaFin!.foundationDate)
        return "\(fechaInicio) - \(fechaFin)"
    }
    
    var mentorshipDateTime: String? {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        
        let fechaInicio = formatter.string(from: subject.fechaInicio!.foundationDate)
        let fechaFin = formatter.string(from: subject.fechaFin!.foundationDate)
        return "\(fechaInicio) - \(fechaFin)"
    }
    
    
    
    enum MyAlerts{
        case success
        case error
    }
    
    var body: some View {
        VStack(alignment: .center) {
           
            Group{
                //profile picture
                Image("marketplace")
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
                    .offset(x: -100 ,y: -50)
                    .frame(width: 170,height: 170)
                    .shadow(radius: 10)
                //name
                Text(subject.nombre!)
                    .font(.title)
                    .offset(x:100,y:-200)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.center)
                    .frame(width: 150)
            }
            
            Group{
                Text(subject.descripcion!)
                    .fixedSize(horizontal: false, vertical: true)
                    .frame(width: 180)
                    .multilineTextAlignment(.center)
                    .font(.subheadline)
                    .offset(x:100,y:-190)
                
                Text(mentorshipDates!)
                    .font(.subheadline)
                    .offset(x:100,y:-180)
                    .frame(width: 207)
                    .multilineTextAlignment(.center)
                
                Text(mentorshipDateTime!)
                    .font(.subheadline)
                    .offset(x:100,y:-170)
                    .frame(width: 207)
                    .foregroundColor(.gray)
                    .multilineTextAlignment(.center)
            }
            
            Group{
                if(subject.esVirtual == true){
                    Text("Virtual")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                        .offset(x:100,y:-160)
                }
                else{
                    Text("Presencial")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                        .offset(x:100,y:-160)
                }
            }
            
            Divider().offset(x:0,y:-140)
            
            Text("Basic Information")
                .offset(x:0,y:-130)
                .font(.title2)
                .multilineTextAlignment(.center)
            
            Group{
                Text("Name: "+(tutor.nombre_apellido ?? "nombre no encontrado"))
                    .offset(x:0,y:-110)
                    .multilineTextAlignment(.center)
                    .font(.headline)
                Text("Capacity: \(subject.capacidad!) estudiantes")
                    .offset(x:0,y:-100)
                    .font(.headline)
                    .multilineTextAlignment(.center)
            }
            
            Group{
                Text("Price: COP $\(subject.precio!, specifier: "%.0f")")
                    .offset(x:0,y:-90)
                    .font(.headline)
                    .multilineTextAlignment(.center)
                if(subject.esVirtual == false){
                    Text("City: "+(subject.ciudad ?? subject.lugar!))
                        .offset(x:0,y:-80)
                        .font(.headline)
                        .multilineTextAlignment(.center)
                }
            }
            
            Group{
                if(subject.esVirtual == true){
                    Text("Platform: "+subject.lugar!)
                        .offset(x:0,y:-80)
                        .font(.headline)
                        .multilineTextAlignment(.center)
                }
                else{
                    Text("Location: "+subject.lugar!)
                        .offset(x:0,y:-70)
                        .font(.headline)
                        .multilineTextAlignment(.center)
                }
            }
            VStack{
                Button(
                    action: {
                        if(isReserved == false){
                            alertType = .success
                            showAlert.toggle()
                            isReserved.toggle()
                        }
                        else{
                            alertType = .error
                            showAlert.toggle()
                            isReserved.toggle()
                        }
                    },
                    label: {
                        Text("Make a reservation")
                            .padding()
                            .foregroundColor(.white)
                            .background(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)))
                            .cornerRadius(10)
                    }).offset(x:0,y:-20).font(.headline)
    
            }.alert(isPresented: $showAlert, content: {
                getAlert()
            })
        }.padding().navigationBarTitle(Text(subject.nombre!), displayMode: .inline)
    }
    
    
    
    private func getAlert() -> Alert{
        switch alertType{
        case.error:
            return Alert(title: Text("An error occured!"))
        case.success:
            return Alert(title: Text("Mentorship booked!"), message: nil, dismissButton: .default(Text("Ok")))
        default:
            return Alert(title: Text("ERROR"))
        }
    }
}
