//
//  ProfileUIView.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 11/12/22.
//

import SwiftUI
import Amplify
import AWSCognitoAuthPlugin
import Reachability

struct ProfileUIView: View {
    
    @State var showAlert: Bool = false
    @State var alertType: MyAlerts? = nil
    
    @State var usuario: Usuario = Usuario()
    var reachability = try! Reachability()
    var pushEditProfileController: ()->Void
    
    enum MyAlerts{
        case success
        case error
    }
    var body: some View {
        ScrollView{
            

        VStack{
            Image("default")
                .resizable()
                .frame(width: 120, height: 120)
                .clipShape(Circle())
            Text(usuario.username ?? "Loading")
                .font(.title)
                .bold()
        }
        Spacer().frame(height: 30)
        VStack(alignment: .leading, spacing: 12){
            HStack{
                Image(systemName: "envelope")
                Text(usuario.email ?? "Loading")
            }
            HStack{
                Image(systemName: "star.fill")
                Text(usuario.calificacion != nil ? String(usuario.calificacion ?? 0.0) : "No raitings yet")
            }
            HStack{
                Image(systemName: "person.fill")
                Text(usuario.nombre_apellido ?? "Loading")
            }
            HStack{
                Image(systemName: "text.justify")
                Text(usuario.biografia ?? "No bio")
            }.frame(maxHeight: 250)
        }.frame(width: 260)
        Spacer().frame(height: 30)
        Button(action:{
            pushEditProfileController()
        } , label: {
            Text("Edit Profile")
                .bold()
                .frame(width: 260, height: 50)
                .cornerRadius(10)
                .foregroundColor(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)))
                .overlay(
                    RoundedRectangle(cornerRadius: 10).stroke(Color.init(UIColor(red: 115/255, green:91/255, blue:242/255, alpha: 1)), lineWidth: 5 ))
        }).disabled((NetworkManager.sharedInstance.reachability).connection == .unavailable)
        Spacer().frame(height: 30)
        Button(action:{
            logoutTapped()
        } , label: {
            Text("Log Out")
                .bold()
                .frame(width: 260, height: 50)
                .background(Color.red)
                .cornerRadius(10)
                .foregroundColor(.white)
        })
        Spacer().frame(height: 30)
        
        }.onAppear(perform: {loadData()})
        
    }
    
    func loadData(){
        self.usuario = CurrentUserService.instance.getCurrentUser()!
    }
    
}



func logoutTapped() {
    // ...
        // after user has successfully logged out
    Amplify.Auth.signOut() { result in
            switch result {
            case .success:
                print("Successfully signed out")
                CurrentUserService.instance.logout()
                pushSignInNavController()
            case .failure(let error):
                print("Sign out failed with error \(error)")
                //self.presentErrorAlert()
            }
        }
}

func editProfileTapped(){
    print("EditProfile Tapped")
}




func pushSignInNavController(){
    DispatchQueue.main.async {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: "LoginNavigationController")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
}

/*
struct ProfileUIView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileUIView()
    }
}*/
