//
//  NotificationsUIView.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 24/09/22.
//

import SwiftUI


struct NotificationsUIView: View {
    var notifications: [Notifications] = []
    var body: some View {
        VStack{
            Text("Notifications").bold().font(.headline)
            List(notifications){
                notification in NotificationCell(notification:notification)
            }
        }
    }
}
/*
struct NotificationsUIView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsUIView(notifications: notiTestData)
    }
}
 */

struct NotificationCell : View {
    let notification: Notifications
    var body: some View {
        return  VStack(alignment: .leading) {
            Text(notification.headline)
                .font(.headline)
                .foregroundColor(.black)

            Text(notification.headline)
                .font(.footnote)
                .foregroundColor(.gray)
        }
        .padding(5)
        
        }
    }

