//
//  Mentoria+Extension.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 6/10/22.
//

import Foundation
import Amplify

extension Mentoria: Identifiable{}

extension Mentoria {
    var mentorshipDates: (fechaInicio:Temporal.DateTime, fechaFin: Temporal.DateTime) {
        (fechaInicio!, fechaFin!)
    }
}
