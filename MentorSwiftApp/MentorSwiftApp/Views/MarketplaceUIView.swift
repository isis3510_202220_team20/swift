//
//  MarketplaceUIView.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import SwiftUI
import Amplify
import AWSPluginsCore
import AWSAPIPlugin
import Reachability


struct MarketplaceUIView: View {
    
    @State var mentorships: [Mentoria] = []
    
    @State var tutor = Usuario()

    
    @State var AllMentorships: [Mentoria] = []
    
    var reachability = try! Reachability()
    
    var body: some View {
        if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
            ZStack{
                Text("Your network is unavailable. Check your data or wifi connection").foregroundColor(.red)
            }
        }
        VStack {
            Text("Marketplace").bold().font(.headline)
            List{
                if(NetworkManager.sharedInstance.reachability).connection != .unavailable{
                    ForEach(mentorships) { mentorship in
                        MarketDetail(subject: mentorship, tutor: tutor, mentorshipDate: mentorship.fechaInicio)
                        
                    }
                }
                else{
                    ForEach(AllMentorships) { mentorship in
                        MarketDetail(subject: mentorship, tutor: tutor, mentorshipDate: mentorship.fechaInicio)
                        
                    }
                }
            }
            Spacer()
        }.onAppear(perform: {
            NetworkManager.isReachable{ _ in
                listMentorships()
            }
            NetworkManager.isUnreachable{ _ in
                updateData()
                getMentorshipList()
            }
        })
    }
    private func listMentorships() {
        DispatchQueue.global(qos:.background).async {
            Amplify.API.query(request: .paginatedList(Mentoria.self)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todos):
                        print("Successfully retrieved list of todos: \(todos)")
                        for mentorship in todos{
                            updateData()
                            self.mentorships.append(mentorship)
                            insertData()
                        }
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
        }
    }
    
    private func insertData(){
        for mentorship in mentorships {
            MentorshipDataStore.shared.insert(id: mentorship.id, nombre: mentorship.nombre ?? "", descripcion: mentorship.descripcion ?? "", topic: mentorship.topic ?? "", fechaInicio: mentorship.fechaInicio!.foundationDate, fechaFin: mentorship.fechaFin!.foundationDate, ciudad: mentorship.ciudad ?? "", lugar: mentorship.lugar ?? "", capacidad: mentorship.capacidad ?? 0, precio: mentorship.precio ?? 0.0, esVirtual: mentorship.esVirtual ?? false, usuarioID: mentorship.usuarioID )
        }
    }
    private func updateData(){
        for mentorship in mentorships {
            MentorshipDataStore.shared.update(id: mentorship.id, nombre: mentorship.nombre ?? "", descripcion: mentorship.descripcion ?? "", topic: mentorship.topic ?? "", fechaInicio: mentorship.fechaInicio!.foundationDate, fechaFin: mentorship.fechaFin!.foundationDate, ciudad: mentorship.ciudad ?? "", lugar: mentorship.lugar ?? "", capacidad: mentorship.capacidad ?? 0, precio: mentorship.precio ?? 0.0, esVirtual: mentorship.esVirtual ?? false, usuarioID: mentorship.usuarioID )
        }
    }
    
    private func getMentorshipList(){
        AllMentorships = MentorshipDataStore.shared.getAllClasses()
    }
}



struct MarketDetail : View {
    let subject: Mentoria
    let tutor: Usuario
    let mentorshipDate: (Temporal.DateTime?)
    
    var mentorshipBeginDate: String? {
        guard let mentorshipDate = self.mentorshipDate else { return nil }
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
            
        let checkIn = formatter.string(from: mentorshipDate.foundationDate)
            return "\(checkIn)"
    }
    var body: some View {
        return NavigationLink(destination: MarketplaceUIViewDetail(subject:subject, tutor: tutor), label: {
            Image("marketplace")
                .resizable()
                .scaledToFit()
                .frame(width: 100,height: 100)
                .cornerRadius(10)
            VStack(alignment: .leading) {
                Text(subject.nombre!)
                    .bold()
                Text(subject.descripcion!)
                    .font(.subheadline)
                    .padding(.top,1)
                Text(mentorshipBeginDate!)
                    .font(.subheadline)
                    .padding(.top,1)
                    .foregroundColor(.gray)
            }
        })
    }
}
