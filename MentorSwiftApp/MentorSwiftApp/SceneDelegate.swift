//
//  SceneDelegate.swift
//  example2
//
//  Created by Juan Andres on 20/09/22.
//

import UIKit
import Amplify
import AWSAPIPlugin

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    var appUser : [String:String]? = [
        "name": "Yasmin",
        "password": "abcd",
        "id": "1"
    ]
    
    var appBecameActiveDate: Date!
    static let uptimeKey = "uptime"
    
    var totalUptime: Double {
        let previousUptime = UserDefaults.standard.double(forKey: SceneDelegate.uptimeKey)
        let sessionTime = appBecameActiveDate.timeIntervalSinceNow
        DispatchQueue.main.async {
            let dataSession = DataSession.init(Date: Temporal.Date.now(), sessionTime: sessionTime.magnitude)
            Amplify.API.mutate(request: .create(dataSession)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let usuario):
                        print("Successfully created Data Session: \(usuario)")
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
            
        }
        return previousUptime - sessionTime
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        // add these lines
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            // if user is logged in before
        if UserDefaults.standard.string(forKey: "username") != nil {
                // instantiate the main tab bar controller and set it as root view controller
                // using the storyboard identifier we set earlier
                let mainTabBarController = storyboard.instantiateViewController(identifier: "TabBarController")
                window?.rootViewController = mainTabBarController
            } else {
                // if user isn't logged in
                // instantiate the navigation controller and set it as root view controller
                // using the storyboard identifier we set earlier
                let loginNavController = storyboard.instantiateViewController(identifier: "LoginNavigationController")
                window?.rootViewController = loginNavController
            }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        appBecameActiveDate = Date()
        print(UserDefaults.standard.double(forKey: SceneDelegate.uptimeKey))
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        UserDefaults.standard.set(totalUptime, forKey: SceneDelegate.uptimeKey)
    }
    
    func changeRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard let window = self.window else {
            return
        }
        
        // change the root view controller to your specific view controller
        window.rootViewController = vc
        
        // add animation
        UIView.transition(with: window,
                          duration: 0.5,
                          options: [.transitionFlipFromLeft],
                          animations: nil,
                          completion: nil)
    }


}

