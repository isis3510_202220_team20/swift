//
//  Marketplace.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//


import SwiftUI

struct Marketplace: Identifiable {
    var id = UUID()
    var name: String
    var headline: String
    var date : String
    var time: String
    var modality: String
    var tutorName: String
    var capacity: String
    var price: String
    var location: String
    var category: String
        
    var imageMentorship: String{ return name}
    var imageName: String { return tutorName }
}

#if DEBUG
let testDataMarketplace = [
    Marketplace(name: "Workout with Ella", headline: "We will do legs and back workout", date: "10 Nov 2022", time: "10:00 - 13:00", modality: "In person", tutorName: "Emily Nelson", capacity: "10 students", price: "COP $20,000", location: "La Gata Golosa", category: "Workout"),
    Marketplace(name: "Linear Algebra", headline: "We will go over the topics for the second midterm.", date: "18 Oct 2022", time: "09:00 - 14:00", modality: "In person", tutorName: "Cristian Jerry", capacity: "40 students", price: "COP $50,000", location: "Cra. 10 #26-21, Hotel Tequendama", category: "Math"),
    Marketplace(name: "Web applications", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "24 Oct 2022", time: "12:00 - 13:00", modality: "Virtual", tutorName: "Camilo Salinas", capacity: "1 student", price: "COP $15,000", location: "Microsoft Teams", category: "Systems Engineer"),
    Marketplace(name: "Amazon Web Services", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "19 Nov 2022", time: "14:00 - 17:00", modality: "In person", tutorName: "Nicolas Bolaños", capacity: "20 students", price: "COP $30,000", location: "Mario Laserna building", category: "Systems Engineering"),
    Marketplace(name: "Business Intelligence", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "28 Nov 2022", time: "08:00 - 11:00", modality: "Virtual", tutorName: "Juan Andrés Santiago", capacity: "20 students", price: "COP $25,000", location: "Zoom", category: "Systems Engineering"),
    Marketplace(name: "Physics 2", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "15 Oct 2022", time: "10:00 - 16:00", modality: "In person", tutorName: "David Álvarez", capacity: "30 students", price: "COP $50,000", location: "Building B", category: "Science"),
    Marketplace(name: "Anadec", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "20 Nov 2022", time: "09:00 - 13:00", modality: "Virtual", tutorName: "Carlos Figueredo", capacity: "20 students", price: "COP $40,000", location: "Zoom", category: "Industrial Engineering"),
]
#endif
