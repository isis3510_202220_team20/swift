//
//  Tutor.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import SwiftUI

struct Tutor: Identifiable {
    var id = UUID()
    var name: String
    var headline: String
    var rating: String
    var bio: String
    var email: String
    var mentorshipG: String
    var mentorshipU: String
    
    var imageName: String { return name }
}

#if DEBUG
let testData = [
    Tutor(name: "Emily Nelson", headline: "Workout", rating: "4.8/5.0", bio: "Personal trainer for 10 years. Worked with schools, universities and companies. Currently teaching o Universidad de los Andes", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Cristian Jerry", headline: "Calculus, Algebra, ANADEC", rating: "4.9/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Camilo Salinas", headline: "Web applications", rating: "5.0/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Nicolas Bolaños", headline: "Amazon Web services", rating: "5.0/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Juan Andrés Santiago", headline: "Business Intelligence, Entreprise Architecture", rating: "4.7/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"juanansantiago2000@hotmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "David Álvarez", headline: "Physics, DALGO", rating: "4.9/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Carlos Figueredo", headline: "Mobile apps, ANADEC", rating: "4.9/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
    Tutor(name: "Juan David Bautista", headline: "API's construction, MOS", rating: "4.8/5.0", bio: "Lorem ipsum dolor sit amet, consectetur adipi scing elit. Tortor turpis sodales nulla velit. Nunc cum vitae, rhoncus leo id.", email:"emily.ranson@gmail.com",mentorshipG: "Workout, Yoga, Stretching",mentorshipU: "Workout"),
]
#endif
