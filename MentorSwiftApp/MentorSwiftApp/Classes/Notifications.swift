//
//  Notification.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 24/09/22.
//

import Foundation

struct Notifications: Identifiable {
    var id = UUID()
    var desc: String
    var headline: String
    var date: String
}

#if DEBUG
let notiTestData = [
    Notifications(desc:"This is Mentor, an app created to help you in your most important subjects", headline:"Welcome to Mentor!", date: "05/08/2022"),
    Notifications(desc:"Try changing your profile picture so people can identify you easier, also try writing a little description in your profile", headline:"How to make your profile more complete", date: "05/08/2022"),
    Notifications(desc:"Your lesson of tomorrow has been moved to a new date", headline:"Change to the schedule of your lesson", date: "05/05/2022"),
    Notifications(desc:"Don't forget to arrive on time so you can take advantage of the time required", headline:"Your lesson will start soon", date: "05/02/2022"),
    
]
#endif

