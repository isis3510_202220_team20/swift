//
//  Classes.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//


import SwiftUI

struct Classes: Identifiable {
    var id = UUID()
    var name: String
    var headline: String
    var date : String
    var time: String
    var modality: String
    var tutorName: String
    var capacity: String
    var price: String
    var location: String
    var category: String
        
    var imageMentorship: String{ return name}
    var imageName: String { return tutorName }
}

#if DEBUG
let testDataMyClasses = [
    Classes(name: "Workout with Ella", headline: "We will do legs and back workout", date: "10 Nov 2022", time: "10:00 - 13:00", modality: "In person", tutorName: "Emily Nelson", capacity: "10 students", price: "COP $20,000", location: "La Gata Golosa", category: "Workout"),
    Classes(name: "Linear Algebra", headline: "We will go over the topics for the second midterm.", date: "18 Oct 2022", time: "09:00 - 14:00", modality: "In person", tutorName: "Cristian Jerry", capacity: "40 students", price: "COP $50,000", location: "Cra. 10 #26-21, Hotel Tequendama", category: "Math"),
    Classes(name: "Amazon Web Services", headline: "Lorem ipsum dolor sit amet, consectetur adipi scing elit.", date: "19 Nov 2022", time: "14:00 - 17:00", modality: "In person", tutorName: "Nicolas Bolaños", capacity: "20 students", price: "COP $30,000", location: "Mario Laserna building", category: "Systems Engineering"),
]
#endif
