//
//  AmplifyUserAdapter.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation

class AmplifyUserAdapter: UserService {
    
    let amplifyUserService = AmplifyUserService()
    
    func createUser(nombre_apellido nombreApellido: String, email: String, biografia: String, calificacion: Double, esProfesor: Bool, username: String){
        let user = Usuario(
            nombre_apellido: nombreApellido,
            email: email,
            biografia: biografia,
            calificacion: calificacion,
            esProfesor: esProfesor,
            username: username
        )
        amplifyUserService.create(user: user)
    }
    
    func getCurrentUser() -> Usuario {
        return amplifyUserService.getCurrentUser()
    }
    
    
}
