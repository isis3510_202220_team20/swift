//
//  AmplifyMentorshipAdapter.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation
import Amplify
import AWSPluginsCore

class AmplifyMentorshipAdapter: MentorshipService {
    
    let amplifyMentorshipService = AmplifyMentorshipService()
    
    func createMentorship(nombre: String, descripcion: String, topic: String, fechaInicio: Date, fechaFin: Date, ciudad: String, lugar: String, precio: Double, capacidad: Int, esVirtual: Bool) {
        
        let currentUser: Usuario = AmplifyUserAdapter().getCurrentUser()
        
        let mentoria = Mentoria(nombre: nombre, descripcion: descripcion, topic: topic, fechaInicio: Temporal.DateTime(fechaInicio), fechaFin: Temporal.DateTime(fechaFin), ciudad: ciudad, lugar: lugar, capacidad: capacidad, precio: precio, esVirtual: esVirtual, usuarioID: currentUser.username!)
        amplifyMentorshipService.create(mentoria: mentoria)
    }
    
    func listMentorships() -> [Mentoria] {
        return amplifyMentorshipService.listAll()
    }
    
    func listMyClasses() -> [Mentoria] {
        return amplifyMentorshipService.listMyClasses()
    }
    
}
