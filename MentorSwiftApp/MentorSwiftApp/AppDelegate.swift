//
//  AppDelegate.swift
//  example2
//
//  Created by Juan Andres on 20/09/22.
//

import UIKit
import Amplify
import AWSPluginsCore
import AWSCognitoAuthPlugin
import AWSAPIPlugin
import AWSDataStorePlugin
import AWSPinpointAnalyticsPlugin
import JWTDecode
import Reachability

    @main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var reachability = try! Reachability()
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
           window?.backgroundColor = UIColor.white
           window?.makeKeyAndVisible()
        do {
            try Amplify.add(plugin: AWSCognitoAuthPlugin())
            try Amplify.add(plugin: AWSPinpointAnalyticsPlugin())
            try Amplify.add(plugin: AWSDataStorePlugin(modelRegistration: AmplifyModels()))
            try Amplify.add(plugin: AWSAPIPlugin())
            print("Successfully configuration of Amplify")
            try Amplify.configure()
            
        } catch {
            print("An error occurred setting up Amplify: \(error)")
        }
        fetchCurrentAuthSession()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func fetchCurrentAuthSession() {
        if ((NetworkManager.sharedInstance.reachability).connection == .unavailable){
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let networkUnavailableVC = storyboard.instantiateViewController(identifier: "homeNointernetVC")
                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(networkUnavailableVC)
                
            }
        } else {
            _ = Amplify.Auth.fetchAuthSession { result in
                switch result {
                case .success(let session):
                    print("Is user signed in - \(session.isSignedIn)")
                    if session.isSignedIn {
                        do {
                            if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                                let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                                UserDefaults.standard.set(tokens.idToken, forKey: "idToken")
                            }
                        } catch {
                            print("Error")
                        }
                        self.pushMainTabBarController()
                    }
                case .failure(let error):
                    print("Fetch session failed with error \(error)")
                    let idToken = UserDefaults.standard.string(forKey: "idToken")
                    do {
                        let jwt = try decode(jwt: idToken!)
                        let expired = jwt.expiresAt?.timeIntervalSinceNow.sign == .minus ? true : false
                        if !expired {
                            self.pushMainTabBarController()
                        }
                    } catch {
                        print("Error")
                    }
                    
                }
            }
        }
    }

    func pushMainTabBarController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "TabBarController")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
    }

}

