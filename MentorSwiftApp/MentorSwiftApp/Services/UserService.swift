//
//  UserService.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation

protocol UserService {
    func createUser(nombre_apellido: String, email: String, biografia: String, calificacion: Double, esProfesor: Bool, username: String)
    
    func getCurrentUser() -> Usuario
}
