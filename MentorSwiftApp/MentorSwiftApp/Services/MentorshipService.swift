//
//  MentorshipService.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation

protocol MentorshipService {
    
    func createMentorship (nombre:String, descripcion: String, topic: String, fechaInicio: Date, fechaFin: Date, ciudad: String, lugar: String, precio: Double, capacidad: Int, esVirtual: Bool)
    
    func listMentorships() -> [Mentoria]
    
    func listMyClasses() -> [Mentoria]
}
