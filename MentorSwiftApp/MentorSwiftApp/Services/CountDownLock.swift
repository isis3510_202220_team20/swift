//
//  CountDownLock.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 12/11/22.
//

import Foundation

final class CountDownLock {
    private var remainingJobs: Int32
    private let isDownloading = DispatchSemaphore(value: 0) // initially locked

    init(count: Int32) {
        remainingJobs = count
    }

    func countDown() {
        OSAtomicDecrement32(&remainingJobs)

        if (remainingJobs == 0) {
            self.isDownloading.signal() // unlock
        }
    }

    func waitUntilZero() {
        self.isDownloading.wait(timeout: DispatchTime.distantFuture)
    }
    func printRemainingJobs(){
        print(remainingJobs)
    }

}
