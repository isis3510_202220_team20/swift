//
//  TutorDataStore.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 11/11/22.
//

import Foundation
import SQLite
import Amplify

class MentorshipDataStore {

    static let DIR_TASK_DB = "MentorshipDB"
    static let STORE_NAME = "mentorship.sqlite3"

    private var db: Connection? = nil
    private let mentorshipss = Table("mentorship")

    
    
    private let id = Expression<String>("id")
    private let nombre = Expression<String>("nombre")
    private let descripcion = Expression<String>("descripcion")
    private let topic = Expression<String>("topic")
    private let fechaInicio = Expression<Date>("fechaInicio")
    private let fechaFin = Expression<Date>("fechaFin")
    private let ciudad = Expression<String>("ciudad")
    private let lugar = Expression<String>("lugar")
    private let capacidad = Expression<Int>("capacidad")
    private let precio = Expression<Double>("precio")
    private let esVirtual = Expression<Bool>("esVirtual")
    private let usuarioID = Expression<String>("usuarioID")


    static let shared = MentorshipDataStore()


    private init() {
        if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dirPath = docDir.appendingPathComponent(Self.DIR_TASK_DB)

            do {
                try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true, attributes: nil)
                let dbPath = dirPath.appendingPathComponent(Self.STORE_NAME).path
                db = try Connection(dbPath)
                createTable()
                print("SQLiteDataStore init successfully at: \(dbPath) ")
            } catch {
                db = nil
                print("SQLiteDataStore init error: \(error)")
            }
        } else {
            db = nil
        }
    }

    private func createTable() {
        guard let database = db else {
            return
        }
        do {
            try database.run(mentorshipss.create { table in
                table.column(id, unique: true)
                table.column(nombre)
                table.column(descripcion)
                table.column(topic)
                table.column(fechaInicio)
                table.column(fechaFin)
                table.column(ciudad)
                table.column(lugar)
                table.column(capacidad)
                table.column(precio)
                table.column(esVirtual)
                table.column(usuarioID)


            })
            print("Table Created...")
        } catch {
            print("table could not be created")
        }
    }

    
    func insert(id: String,nombre: String, descripcion: String ,topic: String ,fechaInicio: Date ,fechaFin: Date ,ciudad: String ,lugar: String ,capacidad: Int ,precio: Double ,esVirtual: Bool ,usuarioID: String) -> Int64? {
        guard let database = db else { return nil }

        let insert = mentorshipss.insert(self.id <- id, self.nombre <- nombre, self.descripcion <- descripcion, self.topic <- topic, self.fechaInicio <- fechaInicio, self.fechaFin <- fechaFin, self.ciudad <- ciudad, self.lugar <- lugar, self.capacidad <- capacidad, self.precio <- precio, self.esVirtual <- esVirtual, self.usuarioID <- usuarioID)

        do {
            let rowID = try database.run(insert)
            return rowID
        } catch {
            print(error)
            return nil
        }
    }
    
    func update(id: String,nombre: String, descripcion: String ,topic: String ,fechaInicio: Date ,fechaFin: Date ,ciudad: String ,lugar: String ,capacidad: Int ,precio: Double ,esVirtual: Bool ,usuarioID: String) -> Bool {
        guard let database = db else { return false }

        _ = mentorshipss.filter(self.id == id)
        do {
            let update = mentorshipss.update([
                self.id <- id, self.nombre <- nombre, self.descripcion <- descripcion, self.topic <- topic, self.fechaInicio <- fechaInicio, self.fechaFin <- fechaFin, self.ciudad <- ciudad, self.lugar <- lugar, self.capacidad <- capacidad, self.precio <- precio, self.esVirtual <- esVirtual, self.usuarioID <- usuarioID])
            if try database.run(update) > 0 {
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
     
    

    
    func getAllClasses() -> [Mentoria] {
        var mentorias: [Mentoria] = []
        guard let database = db else { return [] }
        
        do {
            for mentoria in try database.prepare(self.mentorshipss) {mentorias.append(Mentoria(id: mentoria[id], nombre: mentoria[nombre], descripcion: mentoria[descripcion], topic: mentoria[topic] ,fechaInicio: Temporal.DateTime(mentoria[fechaInicio]), fechaFin: Temporal.DateTime(mentoria[fechaFin]),ciudad: mentoria[ciudad] ,lugar: mentoria[lugar] ,capacidad: mentoria[capacidad] ,precio: mentoria[precio] ,esVirtual: mentoria[esVirtual] ,usuarioID: mentoria[usuarioID]))
            }
        } catch {
            print(error)
        }
        return mentorias
    }
    
    
    func deleteAllUsers() -> [Mentoria] {
        let mentorias: [Mentoria] = []
        guard let database = db else { return [] }

        do {
            for _ in try database.prepare(self.mentorshipss) {
            
            }
        } catch {
            print(error)
        }
        return mentorias
    }

    /*
    func findTask(taskId: Int64) -> Task? {
        var task: Task = Task(id: taskId, name: "", date: Date(), status: false)
        guard let database = db else { return nil }

        let filter = self.tasks.filter(id == taskId)
        do {
            for t in try database.prepare(filter) {
                task.name = t[taskName]
                task.date = t[date]
                task.status = t[status]
            }
        } catch {
            print(error)
        }
        return task
    }
     

    func update(id: Int64, name: String, date: Date = Date(), status: Bool = false) -> Bool {
        guard let database = db else { return false }

        let task = users.filter(self.id == id)
        do {
            let update = task.update([
                taskName <- name,
                self.date <- date,
                self.status <- status
            ])
            if try database.run(update) > 0 {
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
     */

    func delete(id: String) -> Bool {
        guard let database = db else {
            return false
        }
        do {
            let filter = mentorshipss.filter(self.id == id)
            try database.run(filter.delete())
            return true
        } catch {
            print(error)
            return false
        }
    }
    
}
