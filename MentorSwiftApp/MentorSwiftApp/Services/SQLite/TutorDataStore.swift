//
//  TutorDataStore.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 11/11/22.
//

import Foundation
import SQLite

class TutorDataStore {

    static let DIR_TASK_DB = "UserDB"
    static let STORE_NAME = "user.sqlite3"

    private let userss = Table("users")

    private let id = Expression<String>("id")
    private let nombre_apellido = Expression<String>("nombre_apellido")
    private let email = Expression<String>("email")
    private let biografia = Expression<String>("biografia")
    private let calificacion = Expression<Double>("calificacion")
    private let esProfesor = Expression<Bool>("esProfesor")
    private let username = Expression<String>("username")


    static let shared = TutorDataStore()

    private var db: Connection? = nil

    private init() {
        if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dirPath = docDir.appendingPathComponent(Self.DIR_TASK_DB)

            do {
                try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true, attributes: nil)
                let dbPath = dirPath.appendingPathComponent(Self.STORE_NAME).path
                db = try Connection(dbPath)
                createTable()
                print("SQLiteDataStore init successfully at: \(dbPath) ")
            } catch {
                db = nil
                print("SQLiteDataStore init error: \(error)")
            }
        } else {
            db = nil
        }
    }

    private func createTable() {
        guard let database = db else {
            return
        }
        do {
            try database.run(userss.create { table in
                table.column(id, unique: true)
                table.column(nombre_apellido)
                table.column(email)
                table.column(biografia)
                table.column(calificacion)
                table.column(esProfesor)
                table.column(username)
            })
            print("Table Created...")
        } catch {
            print(error)
        }
    }

    
    func insert(id: String, nombre: String,email: String, biografia: String, calificacion: Double, esProfesor: Bool, username: String) -> Int64? {
        guard let database = db else { return nil }

        let insert = userss.insert(self.id <- id, self.nombre_apellido <- nombre, self.email <- email, self.biografia <- biografia, self.calificacion <- calificacion, self.esProfesor <- esProfesor, self.username <- username)

        do {
            let rowID = try database.run(insert)
            return rowID
        } catch {
            print(error)
            return nil
        }
    }
    
    func update(id: String, nombre: String,email: String, biografia: String, calificacion: Double, esProfesor: Bool, username: String) -> Bool {
        guard let database = db else { return false }

        _ = userss.filter(self.id == id)
        do {
            let update = userss.update([
                self.id <- id, self.nombre_apellido <- nombre, self.email <- email, self.biografia <- biografia, self.calificacion <- calificacion, self.esProfesor <- esProfesor, self.username <- username
            ])
            if try database.run(update) > 0 {
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
     
    

    func getAllUsers() -> [Usuario] {
        var users: [Usuario] = []
        guard let database = db else { return [] }

        do {
            for user in try database.prepare(self.userss) {
                users.append(Usuario(id: user[id], nombre_apellido: user[nombre_apellido], email: user[email], biografia: user[biografia], calificacion: user[calificacion], esProfesor: user[esProfesor], username: user[username]))
            }
        } catch {
            print(error)
        }
        return users
    }
    
    func deleteAllUsers() -> [Usuario] {
        let users: [Usuario] = []
        guard let database = db else { return [] }

        do {
            for _ in try database.prepare(self.userss) {
            
            }
        } catch {
            print(error)
        }
        return users
    }

    /*
    func findTask(taskId: Int64) -> Task? {
        var task: Task = Task(id: taskId, name: "", date: Date(), status: false)
        guard let database = db else { return nil }

        let filter = self.tasks.filter(id == taskId)
        do {
            for t in try database.prepare(filter) {
                task.name = t[taskName]
                task.date = t[date]
                task.status = t[status]
            }
        } catch {
            print(error)
        }
        return task
    }
     

    func update(id: Int64, name: String, date: Date = Date(), status: Bool = false) -> Bool {
        guard let database = db else { return false }

        let task = users.filter(self.id == id)
        do {
            let update = task.update([
                taskName <- name,
                self.date <- date,
                self.status <- status
            ])
            if try database.run(update) > 0 {
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
     */

    func delete(id: String) -> Bool {
        guard let database = db else {
            return false
        }
        do {
            let filter = userss.filter(self.id == id)
            try database.run(filter.delete())
            return true
        } catch {
            print(error)
            return false
        }
    }
}
