//
//  CurrentUserService.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 25/10/22.
//

import Foundation
import Amplify
import AWSCognitoAuthPlugin


class UsuarioHolder: NSObject {
    let usuario: Usuario
    init(usuario: Usuario) {
        self.usuario = usuario
    }
}


class CurrentUserService {
 
    public static var instance: CurrentUserService {
        // on first call wait here until callback is done
        // on subsequent calls, no need to wait since already initialized
        return internalInstance
    }
    
    private static let internalInstance = CurrentUserService()
    static let currentUser = CurrentUserService()
    
    private var dbUser: Usuario?
    
    let cache = NSCache<NSString, UsuarioHolder>()

    private let initLatch = CountDownLock(count:1)
    
    private init() {
        initialize()
    }
    
    public func update(){
        
        let userAuth = Amplify.Auth.getCurrentUser()
        let username = userAuth?.username //Saco el username del Amplify AUTH
        let usuario = Usuario.keys
        let predicate = usuario.username == username //Creo el parametro de de filtrado donde busco en la db todos los usuarios (1) que tienen de username el username que saque del AUTH
        Amplify.API.query(request: .paginatedList(Usuario.self, where: predicate)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todos):
                    self.dbUser = todos[0]
                    self.storeInNSCache()
                    
                        
                
                case .failure(_):
                    self.dbUser = nil
                
                    
                }
            case .failure(_):
                self.dbUser = nil
            }
        }
    }
    
    public func initialize() {
    
        let usuarioLocal = retrieveFromNSCache()
        if(usuarioLocal != nil ){
            self.dbUser = usuarioLocal
        }else{
            
     
        
        let userAuth = Amplify.Auth.getCurrentUser()
        let username = userAuth?.username //Saco el username del Amplify AUTH
        let usuario = Usuario.keys
        let predicate = usuario.username == username //Creo el parametro de de filtrado donde busco en la db todos los usuarios (1) que tienen de username el username que saque del AUTH
        Amplify.API.query(request: .paginatedList(Usuario.self, where: predicate)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todos):
                    self.dbUser = todos[0]
                    self.initLatch.countDown()
                    self.storeInNSCache()
                    
                        
                
                case .failure(_):
                    self.dbUser = nil
                    self.initLatch.countDown()
                
                    
                }
            case .failure(_):
                self.dbUser = nil
                self.initLatch.countDown()
            }
        }

        }
       }
    private func storeInNSCache(){
        print("Store in Cache")
        self.cache.setObject(UsuarioHolder(usuario: self.dbUser!), forKey: "usuario")
    }
    public func logout(){
        dbUser = nil
        self.cache.removeAllObjects()
    }
    private func retrieveFromNSCache() -> Usuario?{
        print("From Cache")
        return (cache.object(forKey: "usuario"))?.usuario
    }
    
    public func getCurrentUser() -> Usuario? {
        if (dbUser != nil){
            return dbUser
        }
            initLatch.waitUntilZero()
            // this function should only run after asynch callback was executed
            return dbUser
        }
    
        
}
