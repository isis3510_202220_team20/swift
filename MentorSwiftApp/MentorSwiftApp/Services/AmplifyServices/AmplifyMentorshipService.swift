//
//  AmplifyMentorshipService.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation
import Amplify
import AWSAPIPlugin

class AmplifyMentorshipService {
    func listAll() -> [Mentoria] {
        var mentorships: [Mentoria] = []
        Amplify.API.query(request: .paginatedList(Mentoria.self)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let mentorshipList):
                    print("Successfully retrieved list of todos: \(mentorshipList)")
                    for mentorship in mentorshipList{
                        mentorships.append(mentorship)
                    }
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
        return mentorships
    }
    
    func create(mentoria: Mentoria){
        Amplify.API.mutate(request: .create(mentoria)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let mentoria):
                    print("Successfully created mentorship: \(mentoria)")
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
    }
    
    func listMyClasses() -> [Mentoria] {
        let currentUser: Usuario = AmplifyUserAdapter().getCurrentUser()
        var mentorships: [Mentoria] = []
        Amplify.API.query(request: .getMyClassesQuery(byId: currentUser.id)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let mentorshipList):
                    print("Successfully retrieved list of todos: \(mentorshipList)")
                    for mentorship in mentorshipList{
                        mentorships.append(mentorship)
                    }
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
        return mentorships
    }
    
}

extension GraphQLRequest {
    static func getMyClassesQuery(byId id: String) -> GraphQLRequest<[Mentoria]> {
        let operationName = "listUsuarioMentorias"
        let document = """
        query getStudentClasses($eq: ID!) {
          \(operationName)(filter: {usuarioID: {eq: $eq}}) {
            items {
              mentoria {
                id
                capacidad
                ciudad
                createdAt
                descripcion
                esVirtual
                fechaFin
                fechaInicio
                lugar
                nombre
                precio
                topic
                usuarioID
              }
            }
          }
        }
    """
        return GraphQLRequest<[Mentoria]>(
            document: document,
            variables: ["eq": id],
            responseType: [Mentoria].self,
            decodePath: operationName
        )
    }
}
