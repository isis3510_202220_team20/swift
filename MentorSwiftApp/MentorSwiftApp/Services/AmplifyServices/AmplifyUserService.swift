//
//  AmplifyUserMentoriaService.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 18/11/22.
//

import Foundation
import Amplify
import AWSAPIPlugin
import AWSPluginsCore

class AmplifyUserService{
    
    func create(user: Usuario){
        Amplify.API.mutate(request: .create(user)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let usuario):
                    print("Successfully created usuario: \(usuario)")
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
    }
    
    func getCurrentUser() -> Usuario {
        let user = Amplify.Auth.getCurrentUser()
        
        let username = user?.username
        
        let usuario = Usuario.keys
        let predicate = usuario.username == username
        
        var finalUser = Usuario()
        
        Amplify.API.query(request: .paginatedList(Usuario.self, where: predicate)){ event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let users):
                    finalUser = users[0]
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
        return finalUser
    }
}
