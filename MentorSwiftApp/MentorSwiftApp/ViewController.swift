//
//  ViewController.swift
//  example2
//
//  Created by Juan Andres on 20/09/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBAction func unwindToHomeVC(sender : UIStoryboardSegue){
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func teacherButton(){
        guard let vc = storyboard?.instantiateViewController(identifier:"teacher") as? TeacherViewController else{
            return
        }
        navigationController?.pushViewController(vc, animated: true)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    
    
    

    

}

