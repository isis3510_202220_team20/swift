//
//  teacherViewController.swift
//  example2
//
//  Created by Juan Andres on 20/09/22.
//

import UIKit
import Amplify
import SwiftUI

class TeacherViewController: UIViewController {
    
    @IBOutlet weak var theContainer : UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let childView = UIHostingController(rootView:TutorsUIView())
            addChild(childView)
            childView.view.frame = self.theContainer.bounds
            theContainer.addSubview(childView.view)
        
    }
    
    
    
}
