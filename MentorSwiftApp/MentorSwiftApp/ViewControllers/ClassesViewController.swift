//
//  ClassesViewController.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import UIKit
import SwiftUI

class ClassesViewController: UIViewController {
    
    @IBOutlet weak var theContainer : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let childView = UIHostingController(rootView:ClassesUIView())
        addChild(childView)
        childView.view.frame = theContainer.bounds
        theContainer.addSubview(childView.view)
         
    }

}
