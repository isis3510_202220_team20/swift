//
//  SignUpVC.swift
//  Mentor
//
//  Created by Bolanos, Nicolas on 21/09/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import AWSPluginsCore

class SignUpVC: UIViewController {
    
    let network = NetworkManager.sharedInstance
    
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var pswdTF: UITextField!
    @IBOutlet weak var confPswdTF: UITextField!
    
    @IBOutlet weak var nameErrorLBL: UILabel!
    @IBOutlet weak var emailErrorLBL: UILabel!
    @IBOutlet weak var usernameErrorLBL: UILabel!
    @IBOutlet weak var pswdErrorLBL: UILabel!
    @IBOutlet weak var confPswdLBL: UILabel!
    
    @IBOutlet weak var signUpBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        resetForm()
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)

        }
    }
    
    @IBAction func pushLogInViewController() {
        guard let destinationVC = storyboard?.instantiateViewController(identifier: "SignInVC") as? SignInVC else {
            return
        }
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .flipHorizontal
        present(destinationVC, animated: true)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        signUpBtn.isEnabled = false
        signUp(username: usernameTF.text!, password: pswdTF.text!, email: emailTF.text!)
    }
    @IBAction func signInTapped(_ sender: UIButton) {
        // ...
        // after login is done, maybe put this in the login web service completion block
        resetForm()
//        signUp(username: usernameTF.text!, password: pswdTF.text!, email: emailTF.text!)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let mainTabBarController = storyboard.instantiateViewController(identifier: "TabBarController")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
//        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
    func resetForm(){
        signUpBtn.isEnabled = false
        
        nameErrorLBL.isHidden = true
        emailErrorLBL.isHidden = true
        usernameErrorLBL.isHidden = true
        pswdErrorLBL.isHidden = true
        confPswdLBL.isHidden = true
        
        nameTF.text = ""
        emailTF.text = ""
        usernameTF.text = ""
        pswdTF.text = ""
        confPswdTF.text = ""
    }
    
    @IBAction func nameChanged(_ sender: Any) {
        if let name = nameTF.text
        {
            if let errorMessage = invalidName(name)
            {
                nameErrorLBL.text = errorMessage
                nameErrorLBL.isHidden = false
            }
            else
            {
                nameErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    @IBAction func emailChanged(_ sender: Any) {
        if let email = emailTF.text
        {
            if let errorMessage = invalidEmail(email){
                emailErrorLBL.text = errorMessage
                emailErrorLBL.isHidden = false
            } else {
                emailErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    @IBAction func usernameChanged(_ sender: Any) {
        if let username = usernameTF.text{
            if let errorMessage = invalidUsername(username){
                usernameErrorLBL.text = errorMessage
                usernameErrorLBL.isHidden = false
            } else {
                usernameErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    @IBAction func pswdChanged(_ sender: Any) {
        if let password = pswdTF.text
        {
            if let errorMessage = invalidPassword(password){
                pswdErrorLBL.text = errorMessage
                pswdErrorLBL.isHidden = false
            } else {
                pswdErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    @IBAction func confPswdChanged(_ sender: Any) {
        if let confPassword = confPswdTF.text
        {
            if let errorMessage = invalidConfPassword(confPassword){
                confPswdLBL.text = errorMessage
                confPswdLBL.isHidden = false
            } else {
                confPswdLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    func checkForValidForm(){
        if nameErrorLBL.isHidden && emailErrorLBL.isHidden && usernameErrorLBL.isHidden && pswdErrorLBL.isHidden && confPswdLBL.isHidden {
            signUpBtn.isEnabled = true
        } else {
            signUpBtn.isEnabled = false
        }
    }
    
    func invalidName(_ value: String) -> String? {
        if value == ""{
            return "Required"
        }
        if value.count < 2 {
            return "Name should be at least 2 characters"
        }
        if value.count > 30 {
            return "Name cannot be more than 30 characters"
        }
        return nil
    }
    
    func invalidEmail(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count > 30 {
            return "Email cannot be more than 30 characters"
        }
        let regularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpression)
        if !predicate.evaluate(with: value){
            return "Invalid Email Address"
        }
        return nil
    }
    
    func invalidUsername(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 2 {
            return "Username should be at least 2 characters"
        }
        if value.count > 30 {
            return "Username cannot be more than 30 characters"
        }
        return nil
    }
    
    func invalidPassword(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 8{
            return "Must be at least 8 characters"
        }
        if value.count > 30 {
            return "Cannot be more than 30 characters"
        }
        if containsDigit(value){
            return "Must contain at least 1 digit"
        }
        if containsLowerCase(value){
            return "Must contain a lowercase character"
        }
        if containsUpperCase(value){
            return "Must contain an uppercase character"
        }
        return nil
    }
    
    func invalidConfPassword(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value != pswdTF.text{
            return  "Passwords don't match"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regularExpresion = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regularExpresion = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regularExpresion = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func signUp(username: String, password: String, email: String){
        let userAttributes = [AuthUserAttribute(.email, value: email)]
        let options = AuthSignUpRequest.Options(userAttributes: userAttributes)
        
  
        Amplify.Auth.signUp(username: username, password: password, options: options) { result in
            switch result {
            case .success(let signUpResult):
                if case let .confirmUser(deliveryDetails, _) = signUpResult.nextStep {
                    let amplifyUserAdapter = AmplifyUserAdapter()
                    let nombre_apellido: String = self.nameTF.text!
                    let email: String = self.emailTF.text!
                    let username: String = self.usernameTF.text!
                    DispatchQueue.global(qos: .background).async {
                            amplifyUserAdapter.createUser(nombre_apellido: nombre_apellido,email: email, biografia: "No bio", calificacion: 0, esProfesor: false, username: username)
                    }
                    self.pushConfirmSignUPVC()
                } else {
                    print("SignUp Complete")
                }
            case .failure(let error):
                print("An error occurred while registering a user \(error)")
            }
        }
    }
    
    func pushConfirmSignUPVC (){
        DispatchQueue.main.async {
            guard let destinationVC = self.storyboard?.instantiateViewController(identifier: "ConfirmSignUpVC") as? ConfirmSignUpVC else {
                return
            }
            destinationVC.email = self.emailTF.text!
            destinationVC.username = self.usernameTF.text!
            destinationVC.modalPresentationStyle = .fullScreen
            destinationVC.modalTransitionStyle = .coverVertical
            self.present(destinationVC, animated: true)
        }
    }
}
