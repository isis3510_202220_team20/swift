//
//  ForgotPswdVC.swift
//  Mentor
//
//  Created by Bolanos, Nicolas on 22/09/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin

class ForgotPswdVC: UIViewController {
    
    
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var forgotPswdBtn: UIButton!
    
    @IBOutlet weak var usernameErrorLBL: UILabel!
    
    let errorAlert = UIAlertController(title: "Error",
                                  message: "Please try again",
                                  preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        resetForm()
        errorAlert.addAction(UIAlertAction(title: "Retry",
                                      style: .default,
                                      handler: {_ in self.resetForm() }))
    }
    
    @IBAction func forgotPswdAction(_ sender: Any) {
        Amplify.Auth.resetPassword(for: usernameTF.text!) { result in
            switch result {
            case .success(let resetPasswordResult):
                print("Reset password succeeded.")
                print("Next step: \(resetPasswordResult.nextStep)")
                self.pushResetPasswordController()
            case .failure(let error):
                print("Reset password  failed \(error)")
                self.presentErrorAlert()
            }
        }
    }
    
    
    @IBAction func usernameChanged(_ sender: Any) {
        if let username = usernameTF.text
        {
            if let errorMessage = invalidUsername(username){
                usernameErrorLBL.text = errorMessage
                usernameErrorLBL.isHidden = false
            } else {
                usernameErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    func resetForm(){
        forgotPswdBtn.isEnabled = false
        
        usernameErrorLBL.isHidden = true
        
        usernameTF.text = ""
    }
    
    func checkForValidForm(){
        if usernameErrorLBL.isHidden{
            forgotPswdBtn.isEnabled = true
        } else {
            forgotPswdBtn.isEnabled = false
        }
    }
    
    func invalidUsername(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 2 {
            return "Username should be at least 2 characters"
        }
        if value.count > 30 {
            return "Username cannot be more than 30 characters"
        }
        return nil
    }
    
    func presentErrorAlert(){
        DispatchQueue.main.async {
            self.present(self.errorAlert, animated: true, completion: nil)
        }
    }
    
    func pushResetPasswordController(){
        DispatchQueue.main.async {
            guard let destinationVC = self.storyboard?.instantiateViewController(identifier: "ResetPswdVC") as? ResetPswdVC else {
                return
            }
            destinationVC.username = self.usernameTF.text!
            destinationVC.modalPresentationStyle = .fullScreen
            destinationVC.modalTransitionStyle = .coverVertical
            self.present(destinationVC, animated: true)
        }
    }
}
