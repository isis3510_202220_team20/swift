//
//  ResetPswdVC.swift
//  MentorSwiftApp
//
//  Created by Bolanos, Nicolas on 7/10/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin

class ResetPswdVC: UIViewController {
    var username: String = ""
    
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var newPswdTF: UITextField!
    
    @IBOutlet weak var codeErrorLBL: UILabel!
    @IBOutlet weak var pswdErrorLBL: UILabel!
    
    @IBOutlet weak var updatePswdBtn: UIButton!
    
    let sucessAlert = UIAlertController(title: "Success",
                                  message: "Password updated",
                                  preferredStyle: .alert)
    
    let errorAlert = UIAlertController(title: "Error",
                                  message: "Please try again",
                                  preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetForm()
        
        sucessAlert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: {_ in self.pushSignInViewController() }))
        
        errorAlert.addAction(UIAlertAction(title: "Retry",
                                      style: .default,
                                      handler: {_ in self.resetForm() }))
        
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func updatePswdAction(_ sender: Any) {
        Amplify.Auth.confirmResetPassword(
            for: self.username,
            with: newPswdTF.text!,
            confirmationCode: codeTF.text!
        ) { result in
            switch result {
            case .success:
                print("Password reset confirmed")
                self.presentSuccessAlert()
            case .failure(let error):
                print("Reset password failed with error \(error)")
                self.presentErrorAlert()
            }
        }
    }
    
    @IBAction func codeChanged(_ sender: Any) {
        if let code = codeTF.text
        {
            if let errorMessage = invalidCode(code)
            {
                codeErrorLBL.text = errorMessage
                codeErrorLBL.isHidden = false
            }
            else{
                codeErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    @IBAction func newPswdChanged(_ sender: Any) {
        if let password = newPswdTF.text
        {
            if let errorMessage = invalidPassword(password){
                pswdErrorLBL.text = errorMessage
                pswdErrorLBL.isHidden = false
            } else {
                pswdErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    
    
    func resetForm(){
        updatePswdBtn.isEnabled = false
        
        codeErrorLBL.isHidden = true
        pswdErrorLBL.isHidden = true
        
        codeTF.text = ""
        newPswdTF.text = ""
    }
    
    func checkForValidForm(){
        if codeErrorLBL.isHidden && pswdErrorLBL.isHidden {
            updatePswdBtn.isEnabled = true
        }
        else{
            updatePswdBtn.isEnabled = false
        }
    }
    
    func invalidCode(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count != 6 {
            return "Code should be 6 characters"
        }
        return nil
    }
    
    func invalidPassword(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 8{
            return "Must be at least 8 characters"
        }
        if value.count > 30 {
            return "Cannot be more than 30 characters"
        }
        if containsDigit(value){
            return "Must contain at least 1 digit"
        }
        if containsLowerCase(value){
            return "Must contain a lowercase character"
        }
        if containsUpperCase(value){
            return "Must contain an uppercase character"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regularExpresion = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regularExpresion = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regularExpresion = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func presentSuccessAlert(){
        DispatchQueue.main.async {
            self.present(self.sucessAlert, animated: true, completion: nil)
        }
    }
    
    func presentErrorAlert(){
        DispatchQueue.main.async {
            self.present(self.errorAlert, animated: true, completion: nil)
        }
    }
    
    
    func pushSignInViewController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let signInViewController = storyboard.instantiateViewController(identifier: "SignInVC")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(signInViewController)
        }
    }
}
