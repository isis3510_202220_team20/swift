//
//  EditProfileViewController.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 8/10/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import SwiftUI

class EditProfileVC: UIViewController {
    
    //var sessionManager = didSet {
      //  CurrentUserService.currentUser
    //}
    
    /*
    @IBOutlet weak var usernameLabel: UILabel!
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var bioTextField: UITextField!
    
    @IBOutlet weak var cancelBtn: UIButton!
    */
    
    @IBOutlet var theContainer: UIView!
    
    let network = NetworkManager.sharedInstance
    // var validators = [true, true, true]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        let childView = UIHostingController(rootView: EditProfileUIView(returnFn: backFN))
        addChild(childView)
        childView.view.frame = theContainer.bounds
        theContainer.addSubview(childView.view)
        /*var userDb = CurrentUserService.instance.getCurrentUser()
        if(userDb != nil){
            self.usernameLabel.text = userDb!.username!
            self.nameTextField.text = userDb?.nombre_apellido;
            self.bioTextField.text = userDb?.biografia
            
        }*/
    }
    func backFN(){
        self.dismiss(animated: true)
    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "profileNointernetVC", sender: self)

        }
    }
    /*
    @IBAction func saveBtnEvt(_ sender: Any) {
        updateInfoUsuario()
        
    }
    func updateInfoUsuario(){
       //
       
    }
    
    @IBAction func cancelBtnEvt(_ sender: Any) {
        //cancelar()
        
    }

 
    
    @IBAction func nameChanged(_ sender: Any) {
        
        if let name = nameTextField.text
        {
            if invalidName(name) != nil
            {
               validators[0] = false
            }
            else
            {
                validators[0] = true
            }
        }
        checkForValidForm()
        
    }
    @IBAction func emailChanged(_ sender: Any) {
        if let email = emailTextField.text
        {
            if invalidEmail(email) != nil
            {
               validators[1] = false
            }
            else
            {
                validators[1] = true
            }
        }
        checkForValidForm()
    }
    @IBAction func bioChanged(_ sender: Any) {
        if let bio = bioTextField.text
        {
            if invalidBio(bio) != nil
            {
               validators[2] = false
            }
            else
            {
                validators[2] = true
            }
        }
        checkForValidForm()
    }
    
    
    
    func checkForValidForm(){
        if  validators[0]&&validators[1]&&validators[2]{
            //signUpBtn.isEnabled = true
        } else {
            //print("invalido")
        }
    }
    
    
    func invalidName(_ value: String) -> String? {
        if value == ""{
            return "Required"
        }
        if value.count < 2 {
            return "Name should be at least 2 characters"
        }
        if value.count > 30 {
            return "Name cannot be more than 30 characters"
        }
        return nil
    }
    
    func invalidEmail(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count > 30 {
            return "Email cannot be more than 30 characters"
        }
        let regularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpression)
        if !predicate.evaluate(with: value){
            return "Invalid Email Address"
        }
        return nil
    }
    
    func invalidBio(_ value: String) -> String? {
        if value == ""{
            return "Required"
        }
        if value.count < 6 {
            return "Your bio should be at least 6 characters long"
        }
        if value.count > 240 {
            return "Keep it short, your bio needs to be at most 240 characters long"
        }
        return nil
    }

    
    */
    }

