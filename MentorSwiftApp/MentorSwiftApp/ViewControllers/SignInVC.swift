//
//  SignInVC.swift
//  Mentor
//
//  Created by Bolanos, Nicolas on 21/09/22.
//

import UIKit
import Amplify
import AWSPluginsCore
import AWSCognitoAuthPlugin

class SignInVC: UIViewController {
    
    let network = NetworkManager.sharedInstance

    
    var appUser : [String:String]? = [
        "name": "Yasmin",
        "password": "abcd",
        "id": "1"
    ]
    
    var loginDidStartDate: Date!
    
    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var pswdTF: UITextField!
    
    @IBOutlet weak var usernameErrorLBL: UILabel!
    @IBOutlet weak var pswdErrorLBL: UILabel!
    
    
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var forgotPswdBtn: UIButton!
    
    let errorAlert = UIAlertController(title: "Error",
                                  message: "Invalid username or password",
                                  preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        resetForm()
        errorAlert.addAction(UIAlertAction(title: "Retry",
                                      style: .default,
                                      handler: {_ in self.resetForm() }))
        loginDidStartDate = Date()
    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)

        }
    }
    
    @IBAction func pushSignUpViewController(){
        guard let destinationVC = storyboard?.instantiateViewController(identifier: "SignUpVC") as? SignUpVC else {
            return
        }
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .flipHorizontal
        present(destinationVC, animated: true)
    }
    
    @IBAction func pushForgotPasswordViewController(){
        guard let destinationVC = storyboard?.instantiateViewController(identifier: "ForgotPswdVC") as? ForgotPswdVC else {
            return
        }
        self.navigationController?.pushViewController(destinationVC, animated: true)
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .crossDissolve
        present(destinationVC, animated: true)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        Amplify.Auth.signIn(username: usernameTF.text!, password: pswdTF.text!) { result in
            switch result {
            case .success:
                print("Sign in succeeded")
                _ = Amplify.Auth.fetchAuthSession { result in
                    switch result {
                    case .success(let session):
                        do {
                            if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                                let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                                UserDefaults.standard.set(tokens.idToken, forKey: "idToken")
                                print("Token Saved!")
                            }
                        } catch {
                            print("Error")
                        }
                    case .failure(let error):
                        print("Fetch session failed with error \(error)")
                    }
                }
                self.sendLogInTime()
                self.pushMainTabBarController()
            case .failure(let error):
                print("Sign in failed \(error)")
                self.presentErrorAlert()
            }
        }
    }
    
    @IBAction func usernameChanged(_ sender: Any) {
        if let username = usernameTF.text{
            if let errorMessage = invalidUsername(username){
                usernameErrorLBL.text = errorMessage
                usernameErrorLBL.isHidden = false
            } else {
                usernameErrorLBL.isHidden = true
            }
        }
    }
    
    
    @IBAction func passwordChanged(_ sender: Any) {
        if let password = pswdTF.text
        {
            if let errorMessage = invalidPassword(password){
                pswdErrorLBL.text = errorMessage
                pswdErrorLBL.isHidden = false
            } else {
                pswdErrorLBL.isHidden = true
            }
        }
        checkForValidForm()
    }
    
    func resetForm(){
        logInBtn.isEnabled = false
        
        usernameErrorLBL.isHidden = true
        pswdErrorLBL.isHidden = true
        
        usernameTF.text = ""
        pswdTF.text = ""
    }
    
    func checkForValidForm(){
        if usernameErrorLBL.isHidden && pswdErrorLBL.isHidden {
            logInBtn.isEnabled = true
        } else {
            logInBtn.isEnabled = false
        }
    }
    
    func invalidUsername(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 2 {
            return "Username should be at least 2 characters"
        }
        if value.count > 30 {
            return "Username cannot be more than 30 characters"
        }
        return nil
    }
    
    func invalidPassword(_ value: String) -> String?
    {
        if value == ""{
            return "Required"
        }
        if value.count < 8{
            return "Must be at least 8 characters"
        }
        if value.count > 30 {
            return "Cannot be more than 30 characters"
        }
        if containsDigit(value){
            return "Must contain at least 1 digit"
        }
        if containsLowerCase(value){
            return "Must contain a lowercase character"
        }
        if containsUpperCase(value){
            return "Must contain an uppercase character"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regularExpresion = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regularExpresion = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regularExpresion = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExpresion)
        return !predicate.evaluate(with: value)
    }
     
    func pushMainTabBarController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "TabBarController")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
    }
    
    func sendLogInTime(){
        let loginTime = loginDidStartDate.timeIntervalSinceNow
        DispatchQueue.main.async {
            let dataSession = DataSession.init(Date: Temporal.Date.now(), loginTime: loginTime.magnitude)
            Amplify.API.mutate(request: .create(dataSession)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let usuario):
                        print("Successfully created Data Session: \(usuario)")
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
            
        }

    }
    
    func presentErrorAlert(){
        DispatchQueue.main.async {
            self.present(self.errorAlert, animated: true, completion: nil)
        }
    }
}
