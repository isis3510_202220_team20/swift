//
//  LoginViewController.swift
//  example2
//
//  Created by Juan Andres on 23/09/22.
//

import UIKit

class LoginViewController: UIViewController {
    
    let network = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }

    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)
        }
    }
    
    
    @IBAction func pushLogInViewController() {
        guard let destinationVC = storyboard?.instantiateViewController(identifier: "SignInVC") as? SignInVC else {
            return
        }
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .coverVertical
        present(destinationVC, animated: true)
    }
    
    @IBAction func pushSignUpViewController(){
        guard let destinationVC = storyboard?.instantiateViewController(identifier: "SignUpVC") as? SignUpVC else {
            return
        }
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .coverVertical
        present(destinationVC, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
