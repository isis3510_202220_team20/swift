//
//  ProfileViewController.swift
//  example2
//
//  Created by Juan Andres on 23/09/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import SwiftUI

class ProfileViewController: UIViewController {
    
    var userAuth = Amplify.Auth.getCurrentUser() //Saco el current user de Amplify AUTH
    var userDb: Usuario! 
    let network = NetworkManager.sharedInstance
    
    @IBOutlet var theContainer: UIView!
    
   /* @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet weak var bioLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var profileCard: UIView!
    
    @IBOutlet weak var editProfileBtn: UIButton!
    
    let sucessAlert = UIAlertController(title: "Success",
                                  message: "You have signed out",
                                  preferredStyle: .alert)
    
    let errorAlert = UIAlertController(title: "Error",
                                  message: "Please try again",
                                  preferredStyle: .alert)
*/
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        cargarInfoUsuario()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        let childView = UIHostingController(rootView:ProfileUIView(pushEditProfileController: pushEditProfileController))
        addChild(childView)
        childView.view.frame = theContainer.bounds
        theContainer.addSubview(childView.view)
        /*sucessAlert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: {_ in self.pushSignInNavController() }))
        errorAlert.addAction(UIAlertAction(title: "Retry",
                                      style: .default,
                                      handler: {_ in print("OH! Error") }))*/
        // Do any additional setup after loading the view
    }
    
    
    
    func cargarInfoUsuario(){
        self.userDb = CurrentUserService.instance.getCurrentUser()
       /* if(self.userDb != nil){
            self.username.text = "Hola \( self.userDb.username!) "
            self.emailLabel.text = self.userDb.email
            self.bioLabel.text = self.userDb.biografia ?? "No bio"
            self.ratingLabel.text = "\(String( self.userDb.calificacion ?? 0.0))/5.0"
            self.nameLabel.text = self.userDb.nombre_apellido
        }*/
        
      
    }
    
    
    
    
    /*@IBAction func goToEdit(_ sender: Any) {
        //pushEditProfileController()
    }
    @IBAction func logoutTapped(_ sender: UIButton) {
        // ...
            // after user has successfully logged out
        Amplify.Auth.signOut() { result in
                switch result {
                case .success:
                    print("Successfully signed out")
                    CurrentUserService.instance.logout()
                    self.presentSuccessAlert()
                case .failure(let error):
                    print("Sign out failed with error \(error)")
                    self.presentErrorAlert()
                }
            }
    }
    */
  
   /* func presentSuccessAlert(){
        DispatchQueue.main.async {
            self.present(self.sucessAlert, animated: true, completion: nil)
        }
    }
    
    func presentErrorAlert(){
        DispatchQueue.main.async {
            self.present(self.errorAlert, animated: true, completion: nil)
        }
    }
    */
    func pushSignInNavController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavController = storyboard.instantiateViewController(identifier: "LoginNavigationController")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        }
    }
    

    func pushEditProfileController(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editProfileController = storyboard.instantiateViewController(identifier: "EditProfileVC")
            self.present(editProfileController, animated: true)
        }
    }
    
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "profileNointernetVC", sender: self)

        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
