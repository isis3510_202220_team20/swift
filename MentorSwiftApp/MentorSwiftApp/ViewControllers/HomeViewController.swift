//
//  HomeViewController.swift
//  example2
//
//  Created by Juan Andres on 23/09/22.
//

import UIKit
import CoreLocation
import MapKit
import Amplify
import AWSPinpointAnalyticsPlugin

class HomeViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var userIdLabel: UILabel!
    
    let network = NetworkManager.sharedInstance

    var locationManager: CLLocationManager!
    
    var longitude: Double!
    var latitude: Double!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CurrentUserService.instance.initialize()
        

        // Do any additional setup after loading the view.
        /*
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
         */
         
        if (CLLocationManager.locationServicesEnabled())
        {
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()
        }
        
        
    }
    
    private func showOfflinePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)

        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.longitude = locValue.longitude
        self.latitude = locValue.latitude
        manager.stopUpdatingLocation()
        self.getAddressFromLatLon(pdblLatitude: self.latitude.description, withLongitude: self.longitude.description)
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = Double("\(pdblLongitude)")!
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
                        guard let user = Amplify.Auth.getCurrentUser() else {
                            print("Could not get user, perhaps the user is not signed in")
                            return
                        }
                        
                        let location = AnalyticsUserProfile.Location(latitude: self.latitude,
                                                                     longitude: self.longitude,
                                                                     postalCode: pm.postalCode,
                                                                     city: pm.locality,
                                                                     region: pm.locality,
                                                                     country: pm.country)
                        
                        let userProfile = AnalyticsUserProfile(name: user.username,
                                                               email: "name@example.com",
                                                               location: location)
                        
                        Amplify.Analytics.identifyUser(user.userId, withProfile: userProfile)
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }


                  }
            })

        }
    
    @IBAction func teacherButton(){
        guard let vc = storyboard?.instantiateViewController(identifier:"teacher") as? TeacherViewController else{
            return
        }
        
        navigationController?.pushViewController(vc, animated: true)
        vc.navigationItem.hidesBackButton = true
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func marketplaceButton(){
        guard let vc = storyboard?.instantiateViewController(identifier:"marketplace") as? MarketplaceViewController else{
            return
        }
        navigationController?.pushViewController(vc, animated: true)
        vc.navigationItem.hidesBackButton = true
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func myClassesButton(){
        guard let vc = storyboard?.instantiateViewController(identifier:"myClasses") as? ClassesViewController else{
            return
        }
        navigationController?.pushViewController(vc, animated: true)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func createMentorshipButton(){
        guard let vc = storyboard?.instantiateViewController(identifier:"mentorship") as? CreateMentorshipViewController else{
            return
        }
        navigationController?.pushViewController(vc, animated: true)
        vc.navigationItem.hidesBackButton = true
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
