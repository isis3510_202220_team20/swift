//
//  CalendarViewController.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 4/12/22.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import CalendarKit
import EventKit

class CalendarViewController: DayViewController {
    
    @IBOutlet weak var theContainer: UIView!
    
    private let eventStore = EKEventStore()

    
    override func loadView() {

        dayView = DayView(calendar: calendar)
        view = dayView
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
       
        title = "Calendario"
        navigationController?.navigationBar.isTranslucent = false
        dayView.autoScrollToFirstEvent = true
        
        
        dayView.backgroundColor = UIColor.red
        requestAccessToCalendar()
    }
    
    func requestAccessToCalendar(){
        eventStore.requestAccess(to: .event) { sucess, error in
            
        }
    }
    
    
    override func eventsForDate(_ date: Date)  -> [EventDescriptor]{
        // The `date` always has it's Time components set to 00:00:00 of the day requested
        let startDate = date
        var oneDayComponents = DateComponents()
        oneDayComponents.day = 1
               // By adding one full `day` to the `startDate`, we're getting to the 00:00:00 of the *next* day
        let endDate = calendar.date(byAdding: oneDayComponents, to: startDate)!
        var usuario = CurrentUserService.instance.getCurrentUser()
        let eventos = usuario?.EstudiantesMentoria?.load().elements.map({ relacion -> Event in
            let ckEvent = Event()
            ckEvent.dateInterval.start = relacion.mentoria.fechaInicio!.foundationDate
            ckEvent.dateInterval.end = relacion.mentoria.fechaFin!.foundationDate
            ckEvent.text = (relacion.mentoria.nombre)!
            return ckEvent
            
        })
        
        return eventos!
    }
    
    
}


extension GraphQLRequest {
    static func getMentoriasUsuario(byId id: String) -> GraphQLRequest<Usuario> {
        let document = """
        query getMentorias($id: ID!) {
          getMentorias(id: $id) {
            id
            EstudiantesMentoria {
              items {
                mentoriaID
                mentoria {
                  capacidad
                  fechaInicio
                  fechaFin
                  nombre
                  lugar
                }
              }
            }
          }
        }
        """
        return GraphQLRequest<Usuario>(
            document: document,
            variables: ["id": id],
            responseType: Usuario.self,
            decodePath: "getMentorias"
        )
    }
}
