//
//  CreateMentorshipViewController.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/10/22.
//

import UIKit
import SwiftUI

class CreateMentorshipViewController: UIViewController {
    
    @IBOutlet weak var theContainer : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let childView = UIHostingController(rootView:CreateMentorshipUIView())
        addChild(childView)
        childView.view.frame = theContainer.bounds
        theContainer.addSubview(childView.view)
    }

}
