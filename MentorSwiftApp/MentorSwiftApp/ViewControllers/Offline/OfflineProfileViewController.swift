//
//  OfflineProfileViewController.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 25/11/22.
//


import UIKit

class OfflineProfileViewController: UIViewController {
    
    let network = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        network.reachability.whenReachable = { _ in
            self.showProfilePage()
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func showProfilePage() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "profileViewController", sender: self)

        }
    }
}

