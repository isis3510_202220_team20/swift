//
//  OfflineViewController.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 10/11/22.
//

import UIKit
import Amplify
import AWSPluginsCore

class OfflineHomeViewController: UIViewController {
    
    let network = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        network.reachability.whenReachable = { _ in
            self.showHomePage()
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func showHomePage() -> Void {
        _ = Amplify.Auth.fetchAuthSession{ result in
            switch result {
            case .success(let session):
                print("Is user signed in - \(session.isSignedIn)")
                if session.isSignedIn {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "HomeController", sender: self)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "LoginController", sender: self)
                    }
                }
            case .failure(let error):
                print("Fetch session failed with error \(error)")
            }
        }
        
    }
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */

