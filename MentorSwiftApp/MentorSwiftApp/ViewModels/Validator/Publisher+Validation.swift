//
//  Publisher+Validation.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 21/10/22.
//

import Foundation
import Combine
import Regex

extension Published.Publisher where Value == String {
    
    func nonEmptyValidator(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.nonEmptyValidation(for: self, errorMessage: errorMessage())
    }
    
    func aboveTheLimitName(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.aboveTheLimitName(for: self, errorMessage: errorMessage())
    }
    
    func belowTheLimitName(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.belowTheLimitName(for: self, errorMessage: errorMessage())
    }
    
    func aboveTheLimitDescription(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.aboveTheLimitDescription(for: self, errorMessage: errorMessage())
    }
    
    func belowTheLimitDescription(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.belowTheLimitDescription(for: self, errorMessage: errorMessage())
    }
    
    func aboveTheLimitCity(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.aboveTheLimitCity(for: self, errorMessage: errorMessage())
    }
    
    func belowTheLimitCity(_ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.belowTheLimitCity(for: self, errorMessage: errorMessage())
    }
    
    
    func matcherValidation(_ pattern: String, _ errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.matcherValidation(for: self, withPattern: pattern.r!, errorMessage: errorMessage())
    }
    
}

extension Published.Publisher where Value == Date {
     func dateValidation(afterDate after: Date = .distantPast,
                         beforeDate before: Date = .distantFuture,
                         errorMessage: @autoclosure @escaping ValidationErrorClosure) -> ValidationPublisher {
        return ValidationPublishers.dateValidation(for: self, afterDate: after, beforeDate: before, errorMessage: errorMessage())
    }
}
