//
//  Validation.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 21/10/22.
//

import Foundation

enum Validation {
    case success
    case failure(message: String)
    
    var isSuccess: Bool {
        if case .success = self {
            return true
        }
        return false
    }
}
