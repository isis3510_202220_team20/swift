//
//  ModelMentorClass.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 9/12/22.
//

import Foundation
import UIKit

class ModelMentorClass: ObservableObject{
    
    @Published var startingImage: UIImage? = nil
    @Published var cachedImage: UIImage? = nil
    @Published var infoMessage: String = ""
    let imageName: String = "marketplace"
    let manager = CacheManager.instance
    
    init(){
        getImageFromAssetsFolder()
    }
    
    func getImageFromAssetsFolder(){
        startingImage = UIImage(named: imageName)
    }
    
    func saveToCache(){
        guard let image = startingImage else {return }
        infoMessage = manager.add(image: image, name: imageName)
    }
    
    func getFromCache(){
        
        if let returnedImage = manager.get(name: imageName){
            cachedImage = returnedImage
            infoMessage = "Got image from cache"
        }
        else{
            infoMessage = "Image not found from cache"
        }
        
        cachedImage = manager.get(name: imageName)
    }
    
}
