//
//  ModelCreateMentorship.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 21/10/22.
//

import Foundation
import Combine
import SwiftUI
import Amplify

let postcodeRegex = "[A-Z]{1,2}[0-9]{1,2}[A-Z]?\\s?[0-9][A-Z]{2}"
let nameRegex = "^[A-Za-z-123456789-ñÑáéíóúÁÉÍÓÚ ]+$"
let numberRegex = "^[0-9]"

class ModelCreateMentorship: ObservableObject {
    
    private let limit: Int
    
    // MARK: Published Properties
    
    @Published var name: String = "" {
        didSet {
            UserDefaults.standard.set(name, forKey: "name")
            if name.count > self.limit {
                name = String(name.prefix(self.limit))
            }
        }
    }
    @Published var description: String = "" {
        didSet {
            UserDefaults.standard.set(description, forKey: "description")
            if description.count > self.limit {
                description = String(description.prefix(self.limit))
            }
        }
    }
    @Published var topic: String = "" {
        didSet {
            UserDefaults.standard.set(topic, forKey: "topic")
            if topic.count > self.limit {
                topic = String(topic.prefix(self.limit))
            }
        }
    }
    @Published var city: String = "" {
        didSet {
            UserDefaults.standard.set(city, forKey: "city")
            if city.count > self.limit {
                city = String(city.prefix(self.limit))
            }
        }
    }
    @Published var location: String = ""
    {
        didSet {
            UserDefaults.standard.set(location, forKey: "location")
            if location.count > self.limit {
                location = String(location.prefix(self.limit))
            }
        }
    }
    @Published var capaci : String = ""{
        didSet {
            UserDefaults.standard.set(capaci, forKey: "capaci")
            if capaci.count > self.limit {
                capaci = String(capaci.prefix(self.limit))
            }
        }
    }
    @Published var value : String = ""{
        didSet {
            UserDefaults.standard.set(value, forKey: "value")
            if value.count > self.limit {
                value = String(value.prefix(self.limit))
            }
        }
    }
    
    @Published var isVirtual: Bool = false{
        didSet {
            UserDefaults.standard.set(isVirtual, forKey: "isVirtual")
        }
    }
    
    @Published var dateBegin = Date(){
        didSet {
            UserDefaults.standard.set(dateBegin, forKey: "dateBegin")
        }
    }
    
    @Published var dateEnd = Date(){
        didSet {
            UserDefaults.standard.set(dateEnd, forKey: "dateEnd")
        }
    }
    
    
    init(limit: Int) {
        self.name = UserDefaults.standard.string(forKey: "name") ?? ""
        self.description = UserDefaults.standard.string(forKey: "description") ?? ""
        self.topic = UserDefaults.standard.string(forKey: "topic") ?? ""
        self.city = UserDefaults.standard.string(forKey: "city") ?? ""
        self.location = UserDefaults.standard.string(forKey: "location") ?? ""
        self.capaci = UserDefaults.standard.string(forKey: "capaci") ?? ""
        self.value = UserDefaults.standard.string(forKey: "value") ?? ""
        self.isVirtual = UserDefaults.standard.bool(forKey: "isVirtual")
        
        self.dateBegin = (UserDefaults.standard.object(forKey: "dateBegin") as? Date ?? Date.now)
        self.dateEnd = (UserDefaults.standard.object(forKey: "dateEnd") as? Date ?? Date.now)
        
        
        self.limit = limit
    }
    
    
    
    // Information
    //name
    lazy var nameValidation: ValidationPublisher = {
        $name.nonEmptyValidator("Mentorship name required")
    }()
    
    lazy var aboveTheLimitName: ValidationPublisher = {
        $name.aboveTheLimitName("Maximum length is 25 characters")
    }()
    
    lazy var belowTheLimitName: ValidationPublisher = {
        $name.belowTheLimitName("Minimum length is 2")
    }()
    
    
    lazy var nameLetters: ValidationPublisher = {
        $name.matcherValidation(nameRegex, "Name is not valid")
    }()
    
    
    //description
    lazy var descriptionValidation: ValidationPublisher = {
        $description.nonEmptyValidator("Description is required")
    }()
    
    lazy var aboveTheLimitDescription: ValidationPublisher = {
        $description.aboveTheLimitDescription("Maximum length is 50 characters")
    }()
    
    lazy var belowTheLimitDescription: ValidationPublisher = {
        $description.belowTheLimitDescription("Minimum lenght is 5")
    }()
    
    
    // topic
    lazy var tutorsNameValidation: ValidationPublisher = {
        $topic.nonEmptyValidator("Category is required")
    }()
    
    lazy var aboveTheLimitTopic: ValidationPublisher = {
        $topic.aboveTheLimitName("Maximum length is 25 characters")
    }()
    
    lazy var belowTheLimitTopic: ValidationPublisher = {
        $topic.belowTheLimitName("Minimum length is 2")
    }()
    
    lazy var nameTutorLetters: ValidationPublisher = {
        $topic.matcherValidation(nameRegex, "Name is not valid")
    }()
    
    
    
    lazy var capacityValidation: ValidationPublisher = {
        $capaci.matcherValidation(numberRegex, "The input must be a number")
    }()
    
    //city
    lazy var cityValidation: ValidationPublisher = {
        $city.nonEmptyValidator("City is required")
    }()
    
    lazy var aboveTheLimitCity: ValidationPublisher = {
        $topic.aboveTheLimitCity("Maximum length is 25 characters")
    }()
    
    lazy var belowTheLimitCity: ValidationPublisher = {
        $topic.belowTheLimitCity("Minimum length is 2")
    }()
    
    lazy var cityLetters: ValidationPublisher = {
        $city.matcherValidation(nameRegex, "City is not valid")
    }()
    
    //location
    lazy var locationValidation: ValidationPublisher = {
        $location.nonEmptyValidator("The location of the mentorship must be provided")
    }()
    
    lazy var aboveTheLimitLocation: ValidationPublisher = {
        $topic.aboveTheLimitCity("Maximum length is 25 characters")
    }()
    
    lazy var belowTheLimitLocation: ValidationPublisher = {
        $topic.belowTheLimitCity("Minimum length is 2")
    }()
    
    
    lazy var priceValidation: ValidationPublisher = {
        $value.matcherValidation(numberRegex, "The input must be a number")
    }()
    
    
    func createMentorship(nombre:String, descripcion: String, topic: String, fechaInicio: Temporal.DateTime, fechaFin: Temporal.DateTime, ciudad: String, lugar: String, precio: Double, capacidad: Int, esVirtual: Bool) {
        
        let user = Amplify.Auth.getCurrentUser()
        var userTutor: Usuario!
        
        let username = user?.username
        
        let usuario = Usuario.keys
        let predicate = usuario.username == username
        Amplify.API.query(request: .paginatedList(Usuario.self, where: predicate)){ event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todos):
                    print("Successfully retrieved list of todos: \(todos)")
                    userTutor = todos[0]
                    var tutor = userTutor
                    
                    let mentoria = Mentoria(nombre: nombre, descripcion: descripcion, topic: topic, fechaInicio: fechaInicio, fechaFin: fechaFin, ciudad: ciudad, lugar: lugar, capacidad: capacidad, precio: precio, esVirtual: esVirtual, usuarioID: tutor!.id)
                    
                    let userMentoria = UsuarioMentoria(mentoria: mentoria, usuario: tutor!)
                    
                    Amplify.API.mutate(request: .create(userMentoria)) { event in
                        switch event {
                        case .success(let result):
                            switch result {
                            case .success(let userMentoria):
                                print("Successfully created userMentoria: \(userMentoria)")
                            case .failure(let error):
                                print("Got failed result with \(error.errorDescription)")
                            }
                        case .failure(let error):
                            print("Got failed event with error \(error)")
                        }
                    }
                    
                    Amplify.API.mutate(request: .create(mentoria)) { event in
                        switch event {
                        case .success(let result):
                            switch result {
                            case .success(let mentoria):
                                print("Successfully created mentorship: \(mentoria)")
                            case .failure(let error):
                                print("Got failed result with \(error.errorDescription)")
                            }
                        case .failure(let error):
                            print("Got failed event with error \(error)")
                        }
                    }
                    
                    tutor?.esProfesor = true
                    Amplify.API.mutate(request: .update(tutor!)) { event in
                        switch event {
                        case .success(let result):
                            switch result {
                            case .success(let tutor):
                                print("Successfully updated tutor: \(tutor)")
                            case .failure(let error):
                                print("Got failed result with \(error.errorDescription)")
                            }
                        case .failure(let error):
                            print("Got failed event with error \(error)")
                        }
                    }
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
    }
}

