//
//  ModelTutor.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 11/11/22.
//

import Foundation
import UIKit
import Amplify

@MainActor class ModelTutor: ObservableObject {

    @Published var startingImage: UIImage? = nil
    @Published var cachedImage: UIImage? = nil
    @Published var infoMessage: String = ""
    @Published var tutors: [Usuario] = []
    @Published var allUser: [Usuario] = []
    let imageName: String = "default"
    let manager = CacheManager.instance
    
    init(){
        getImageFromAssetsFolder()
    }
    
    func getImageFromAssetsFolder(){
        startingImage = UIImage(named: imageName)
    }
    
    func saveToCache(){
        guard let image = startingImage else {return }
        infoMessage = manager.add(image: image, name: imageName)
    }
    
    func getFromCache(){
        
        if let returnedImage = manager.get(name: imageName){
            cachedImage = returnedImage
            infoMessage = "Got image from cache"
        }
        else{
            infoMessage = "Image not found from cache"
        }
        
        cachedImage = manager.get(name: imageName)
    }
    
    func listTutors() {
            Amplify.API.query(request: .paginatedList(Usuario.self)){ event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todos):
                        print("Successfully retrieved list of todos: \(todos)")
                        DispatchQueue.main.async {
                            self.tutors = todos.filter({tutor in
                               
                                  
                                return tutor.esProfesor == true
                         
                            })
                        }
                        self.updateData()
                        self.insertData()
                    case .failure(let error):
                        print("Got failed result with \(error.errorDescription)")
                    }
                case .failure(let error):
                    print("Got failed event with error \(error)")
                }
            }
    }
    
    func insertData(){
        for tutor in tutors{
            TutorDataStore.shared.insert(id: tutor.id, nombre: tutor.nombre_apellido ?? "", email: tutor.email ?? "", biografia: tutor.biografia ?? "", calificacion: tutor.calificacion ?? 0.0, esProfesor: tutor.esProfesor ?? false , username: tutor.username ?? "")
        }
    }
    
    func updateData(){
        for tutor in tutors{
            TutorDataStore.shared.update(id: tutor.id, nombre: tutor.nombre_apellido ?? "", email: tutor.email ?? "", biografia: tutor.biografia ?? "", calificacion: tutor.calificacion ?? 0.0, esProfesor: tutor.esProfesor ?? false , username: tutor.username ?? "")
        }
    }
    
    func getUserList() {
        allUser = TutorDataStore.shared.getAllUsers()
    }

}
