//
//  MockedStorage.swift
//  MentorSwiftApp
//
//  Created by Juan Andres on 3/11/22.
//

import Foundation

class MockedStorage: Storage {
    var memory: [String: Any?] = [:]
  
    func string(forKey key: String) -> String? {
        memory[key] as? String
    }
    
    func set(_ value: String?, forKey key: String) {
        memory[key] = value
    }
}
