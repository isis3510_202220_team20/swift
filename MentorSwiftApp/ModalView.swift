//
//  ModalView.swift
//  MentorSwiftApp
//
//  Created by Camilo Andres Salinas Martinez on 12/12/22.
//

import SwiftUI

struct BioGenerada: Hashable, Codable{
    let result:String
    init(){
        result = ""
    }
}

class ViewModel: ObservableObject{
    @Published var bioGenerada:BioGenerada = BioGenerada()
    @Published var generando: Bool = false
    func fetch(nombre:String, subject1:String, subject2: String, subject3:String){
        DispatchQueue.main.async {
            self.generando = true}
        print(nombre, subject1, subject2, subject3)
        let urlString = ("https://h6ddu7nj6qriy45lezsnxw2r7e0magwz.lambda-url.us-east-1.on.aws?nombre="+nombre+"&s1="+subject1+"&s2="+subject2+"&s3="+subject3).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
       let url = URL(string: urlString!)
    
        
        let task = URLSession.shared.dataTask(with: url!){[weak self] data, _, error in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    self?.generando = false}
                return
            }

            do{
                let respuesta = try JSONDecoder().decode(BioGenerada.self, from: data)
                DispatchQueue.main.async {
                    self?.bioGenerada = respuesta
                    self?.generando = false
                }
                
                
            }
            catch{
                DispatchQueue.main.async {
                    self?.generando = false}
                print(error)
            }
        }

        task.resume()
        
    }
}


struct ModalView: View {
    @Binding var isShowing: Bool
    @Binding var bio: String
    @StateObject var viewModel = ViewModel()
    
    @State var subject1 = ""
    @State var subject2 = ""
    @State var subject3 = ""
    var body: some View {
        ZStack(alignment: .bottom){
            if(isShowing){
                Color.black
                    .opacity(0.3)
                    .ignoresSafeArea()
                    .onTapGesture {
                        isShowing = false
                    }
                VStack{
                    Text("To generate your custom bio please fill 3 subjects or things you're going to teach in mentor")
                    Form {
                        Section(header: Text("Subject #1")){
                            TextField("Subject 1",text: $subject1)
                        }
                        Section(header: Text("Subject #2")){
                            TextField("Subject 2",text: $subject2)
                        }
                        Section(header: Text("Subject #3")){
                            TextField("Subject 3",text: $subject3)
                        }
                        if(viewModel.generando){
                            Text("Generating Bio...")
                        }else{
                            Text("Fill the subjects and press generate")
                        }
                       
                        Text(viewModel.bioGenerada.result != "" ? viewModel.bioGenerada.result.dropFirst(2) : "Result will appear here")
                        
           
                            
                            Button(action: {
                                
                                viewModel.fetch(nombre: CurrentUserService.instance.getCurrentUser()!.nombre_apellido! , subject1: subject1, subject2: subject2, subject3: subject3)
                                
                            }, label: {
                                Text("Generate")
                            }).disabled(subject1 == "" || subject2 == "" || subject3 == "" || viewModel.generando).accentColor(Color.purple)
                    
                        Button(action: {
                            var curBio = viewModel.bioGenerada.result
                            curBio.removeFirst(2)
                            bio = curBio
                            isShowing = false
                            
                        }, label: {
                            Text("Save")
                        }).disabled( viewModel.generando || viewModel.bioGenerada.result == "").foregroundColor(Color.purple)
                        
            
                        Button(action: {
                            
                           
                            isShowing = false
                            
                        }, label: {
                            Text("Cancel")
                        }).foregroundColor(Color.red)
                       
                    }
                  
                }
                .background(Color.clear)
                .frame(height: 700)
                .frame(maxWidth: .infinity)
                .padding()
                .background(Color.white)
                .transition(.move(edge: .bottom))
            }
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom).ignoresSafeArea().animation(.easeInOut).onAppear()
    }
}




/*
struct ModalView_Previews: PreviewProvider {
    static var previews: some View {
        ModalView()
    }
}
*/
